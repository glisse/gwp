#!/bin/sh
target=$1

numberstring=`printf "%02d" $target`
git rm pxa--p$numberstring*

for i in {1..20} ; do
    if [[ $i -le $target ]] ; then
        continue
    fi

    numberstring=`printf "%02d" $i`
    prev=$((i - 1))
    prevnumberstring=`printf "%02d" $prev`

    any=`find . -name "pxa--p$numberstring*" -print -quit`
    if test -z "$any" ; then
        continue
    fi

    for f in `ls pxa--p$numberstring*`; do
        t=`echo $f | sed -e "s/--p$numberstring/--p$prevnumberstring/"`
        git mv $f $t
    done
    sed -ibak -e "s/--p$numberstring/--p$prevnumberstring/" pxa--p$prevnumberstring.sh
    rm -f *shbak
done
