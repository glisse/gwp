#!/bin/sh
after=$1
for i in {20..1} ; do
    if [[ $i -le $after ]] ; then
        continue
    fi

    numberstring=`printf "%02d" $i`
    next=$((i + 1))
    nextnumberstring=`printf "%02d" $next`

    any=`find . -name "pxa--p$numberstring*" -print -quit`
    if test -z "$any" ; then
        continue
    fi

    for f in `ls pxa--p$numberstring*`; do
        t=`echo $f | sed -e "s/--p$numberstring/--p$nextnumberstring/"`
        git mv $f $t
    done
    sed -ibak -e "s/--p$numberstring/--p$nextnumberstring/" pxa--p$nextnumberstring.sh
    rm -f *shbak
done
