From 3ce77b8f350b2cd5fd14756c6b36894089e9f2f2 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?J=C3=A9r=C3=B4me=20Glisse?= <jglisse@redhat.com>
Date: Thu, 28 May 2020 12:10:55 -0400
Subject: [PATCH] mm: hide struct page.index field
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Everyone should be using the correct helper now, only way to make sure
everyone get educated about it is to rename the field.

Signed-off-by: Jérôme Glisse <jglisse@redhat.com>
Cc: linux-fsdevel@vger.kernel.org
Cc: linux-mm@kvack.org
Cc: Alexander Viro <viro@zeniv.linux.org.uk>
Cc: Josef Bacik <jbacik@fb.com>
Cc: Tejun Heo <tj@kernel.org>
Cc: Jan Kara <jack@suse.cz>
Cc: Andrew Morton <akpm@linux-foundation.org>
---
 include/linux/mm_types.h |  4 ++--
 include/linux/page-xa.h  | 16 ++++++++--------
 2 files changed, 10 insertions(+), 10 deletions(-)

diff --git a/include/linux/mm_types.h b/include/linux/mm_types.h
index 9b98973b3da9..b2b9a308f7e7 100644
--- a/include/linux/mm_types.h
+++ b/include/linux/mm_types.h
@@ -85,12 +85,12 @@ struct page {
 			/* See page-flags.h for PAGE_MAPPING_FLAGS */
 			struct address_space *mapping;
 			/*
-			 * @index: Offset within mapping.
+			 * @_index: Offset within mapping.
 			 *
 			 * DO NOT ACCESS DIRECTLY ! USE THE APPROPRIATE
 			 * *page_index* HELPERS ! See page-xa.h
 			 */
-			pgoff_t index;
+			pgoff_t _index;
 			/**
 			 * @private: Mapping-private opaque data.
 			 * Usually used for buffer_heads if PagePrivate.
diff --git a/include/linux/page-xa.h b/include/linux/page-xa.h
index 33d6035b9a6f..b72845b2dc5f 100644
--- a/include/linux/page-xa.h
+++ b/include/linux/page-xa.h
@@ -82,7 +82,7 @@ static inline bool PageXA(struct page *page)
  */
 static inline pgoff_t raw_page_index(struct page *page)
 {
-	return page->index;
+	return page->_index;
 }
 
 /**
@@ -98,7 +98,7 @@ static inline pgoff_t raw_page_index(struct page *page)
  */
 static inline void raw_page_index_set(struct page *page, pgoff_t index)
 {
-	page->index = index;
+	page->_index = index;
 }
 
 /**
@@ -129,10 +129,10 @@ static inline pgoff_t anon_page_index(struct anon_vma *anon_vma,
 	}
 
 	if (!PageCompound(page)) {
-		return page->index;
+		return page->_index;
 	}
 
-	pgoff = compound_head(page)->index;
+	pgoff = compound_head(page)->_index;
 	pgoff += page - compound_head(page);
 	return pgoff;
 }
@@ -160,7 +160,7 @@ static inline void anon_page_index_set(struct anon_vma *anon_vma,
 		VM_BUG_ON_PAGE(PageXA(page), page);
 	}
 
-	page->index = pgoff;
+	page->_index = pgoff;
 }
 
 extern pgoff_t swapfile_page_index(struct page *page);
@@ -211,7 +211,7 @@ static inline pgoff_t fs_page_index(struct address_space *mapping,
 	 * about differencing between THP and hugetlbfs.
 	 */
 	if (likely(!PageTail(page))) {
-		return page->index;
+		return page->_index;
 	}
 
 	page_head = compound_head(page);
@@ -236,7 +236,7 @@ static inline pgoff_t fs_page_index(struct address_space *mapping,
 	else
 		pgoff = page - page_head;
 
-	return pgoff + page_head->index;
+	return pgoff + page_head->_index;
 }
 
 /**
@@ -281,7 +281,7 @@ static inline void fs_page_index_set(struct address_space *mapping,
 		VM_BUG_ON_PAGE(PageXA(page), page);
 	}
 
-	page->index = pgoff;
+	page->_index = pgoff;
 }
 
 /**
-- 
2.26.2

