From 589364bb7f98d8e709a6f23fbf1935f4a24dc0c1 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?J=C3=A9r=C3=B4me=20Glisse?= <jglisse@redhat.com>
Date: Sat, 30 May 2020 16:21:23 -0400
Subject: [PATCH] mm/z3fold: use raw_page_private*()
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Page use by z3fold are just raw (not map into a process) so just
use raw_page* helpers.

Signed-off-by: Jérôme Glisse <jglisse@redhat.com>
Cc: Uladzislau Rezki <uladzislau.rezki@sony.com>
Cc: Vitaly Wool <vitaly.wool@konsulko.com>
Cc: Andrew Morton <akpm@linux-foundation.org>
Cc: Qian Cai <cai@lca.pw>
Cc: Raymond Jennings <shentino@gmail.com>
Cc: linux-fsdevel@vger.kernel.org
Cc: linux-mm@kvack.org
Cc: Alexander Viro <viro@zeniv.linux.org.uk>
Cc: Tejun Heo <tj@kernel.org>
Cc: Jan Kara <jack@suse.cz>
Cc: Josef Bacik <jbacik@fb.com>
Cc: Andrew Morton <akpm@linux-foundation.org>
---
 mm/z3fold.c | 99 +++++++++++++++++++++++++++++------------------------
 1 file changed, 54 insertions(+), 45 deletions(-)

diff --git a/mm/z3fold.c b/mm/z3fold.c
index 8c3bb5e508b8..f589282a6f66 100644
--- a/mm/z3fold.c
+++ b/mm/z3fold.c
@@ -297,7 +297,7 @@ static inline void put_z3fold_header(struct z3fold_header *zhdr)
 {
 	struct page *page = virt_to_page(zhdr);
 
-	if (!test_bit(PAGE_HEADLESS, &page->private))
+	if (!test_bit(PAGE_HEADLESS, raw_page_private_ptr(page)))
 		z3fold_page_unlock(zhdr);
 }
 
@@ -401,11 +401,11 @@ static struct z3fold_header *init_z3fold_page(struct page *page, bool headless,
 	struct z3fold_buddy_slots *slots;
 
 	INIT_LIST_HEAD(&page->lru);
-	clear_bit(PAGE_HEADLESS, &page->private);
-	clear_bit(MIDDLE_CHUNK_MAPPED, &page->private);
-	clear_bit(NEEDS_COMPACTING, &page->private);
-	clear_bit(PAGE_STALE, &page->private);
-	clear_bit(PAGE_CLAIMED, &page->private);
+	clear_bit(PAGE_HEADLESS, raw_page_private_ptr(page));
+	clear_bit(MIDDLE_CHUNK_MAPPED, raw_page_private_ptr(page));
+	clear_bit(NEEDS_COMPACTING, raw_page_private_ptr(page));
+	clear_bit(PAGE_STALE, raw_page_private_ptr(page));
+	clear_bit(PAGE_CLAIMED, raw_page_private_ptr(page));
 	if (headless)
 		return zhdr;
 
@@ -527,8 +527,8 @@ static void __release_z3fold_page(struct z3fold_header *zhdr, bool locked)
 	int i;
 
 	WARN_ON(!list_empty(&zhdr->buddy));
-	set_bit(PAGE_STALE, &page->private);
-	clear_bit(NEEDS_COMPACTING, &page->private);
+	set_bit(PAGE_STALE, raw_page_private_ptr(page));
+	clear_bit(NEEDS_COMPACTING, raw_page_private_ptr(page));
 	spin_lock(&pool->lock);
 	if (!list_empty(&page->lru))
 		list_del_init(&page->lru);
@@ -599,7 +599,7 @@ static void free_pages_work(struct work_struct *w)
 		struct page *page = virt_to_page(zhdr);
 
 		list_del(&zhdr->buddy);
-		if (WARN_ON(!test_bit(PAGE_STALE, &page->private)))
+		if (WARN_ON(!test_bit(PAGE_STALE, raw_page_private_ptr(page))))
 			continue;
 		spin_unlock(&pool->stale_lock);
 		cancel_work_sync(&zhdr->work);
@@ -779,7 +779,7 @@ static int z3fold_compact_page(struct z3fold_header *zhdr)
 {
 	struct page *page = virt_to_page(zhdr);
 
-	if (test_bit(MIDDLE_CHUNK_MAPPED, &page->private))
+	if (test_bit(MIDDLE_CHUNK_MAPPED, raw_page_private_ptr(page)))
 		return 0; /* can't move middle chunk, it's used */
 
 	if (unlikely(PageIsolated(page)))
@@ -832,7 +832,8 @@ static void do_compact_page(struct z3fold_header *zhdr, bool locked)
 		WARN_ON(z3fold_page_trylock(zhdr));
 	else
 		z3fold_page_lock(zhdr);
-	if (WARN_ON(!test_and_clear_bit(NEEDS_COMPACTING, &page->private))) {
+	if (WARN_ON(!test_and_clear_bit(NEEDS_COMPACTING,
+					raw_page_private_ptr(page)))) {
 		z3fold_page_unlock(zhdr);
 		return;
 	}
@@ -846,8 +847,8 @@ static void do_compact_page(struct z3fold_header *zhdr, bool locked)
 	}
 
 	if (unlikely(PageIsolated(page) ||
-		     test_bit(PAGE_CLAIMED, &page->private) ||
-		     test_bit(PAGE_STALE, &page->private))) {
+		     test_bit(PAGE_CLAIMED, raw_page_private_ptr(page)) ||
+		     test_bit(PAGE_STALE, raw_page_private_ptr(page)))) {
 		z3fold_page_unlock(zhdr);
 		return;
 	}
@@ -913,8 +914,8 @@ static inline struct z3fold_header *__z3fold_alloc(struct z3fold_pool *pool,
 		spin_unlock(&pool->lock);
 
 		page = virt_to_page(zhdr);
-		if (test_bit(NEEDS_COMPACTING, &page->private) ||
-		    test_bit(PAGE_CLAIMED, &page->private)) {
+		if (test_bit(NEEDS_COMPACTING, raw_page_private_ptr(page)) ||
+		    test_bit(PAGE_CLAIMED, raw_page_private_ptr(page))) {
 			z3fold_page_unlock(zhdr);
 			zhdr = NULL;
 			put_cpu_ptr(pool->unbuddied);
@@ -958,8 +959,10 @@ static inline struct z3fold_header *__z3fold_alloc(struct z3fold_pool *pool,
 			spin_unlock(&pool->lock);
 
 			page = virt_to_page(zhdr);
-			if (test_bit(NEEDS_COMPACTING, &page->private) ||
-			    test_bit(PAGE_CLAIMED, &page->private)) {
+			if (test_bit(NEEDS_COMPACTING,
+				     raw_page_private_ptr(page)) ||
+			    test_bit(PAGE_CLAIMED,
+				     raw_page_private_ptr(page))) {
 				z3fold_page_unlock(zhdr);
 				zhdr = NULL;
 				if (can_sleep)
@@ -1166,7 +1169,7 @@ static int z3fold_alloc(struct z3fold_pool *pool, size_t size, gfp_t gfp,
 	atomic64_inc(&pool->pages_nr);
 
 	if (bud == HEADLESS) {
-		set_bit(PAGE_HEADLESS, &page->private);
+		set_bit(PAGE_HEADLESS, raw_page_private_ptr(page));
 		goto headless;
 	}
 	if (can_sleep) {
@@ -1227,9 +1230,10 @@ static void z3fold_free(struct z3fold_pool *pool, unsigned long handle)
 
 	zhdr = get_z3fold_header(handle);
 	page = virt_to_page(zhdr);
-	page_claimed = test_and_set_bit(PAGE_CLAIMED, &page->private);
+	page_claimed = test_and_set_bit(PAGE_CLAIMED,
+					raw_page_private_ptr(page));
 
-	if (test_bit(PAGE_HEADLESS, &page->private)) {
+	if (test_bit(PAGE_HEADLESS, raw_page_private_ptr(page))) {
 		/* if a headless page is under reclaim, just leave.
 		 * NB: we use test_and_set_bit for a reason: if the bit
 		 * has not been set before, we release this page
@@ -1263,7 +1267,7 @@ static void z3fold_free(struct z3fold_pool *pool, unsigned long handle)
 		pr_err("%s: unknown bud %d\n", __func__, bud);
 		WARN_ON(1);
 		put_z3fold_header(zhdr);
-		clear_bit(PAGE_CLAIMED, &page->private);
+		clear_bit(PAGE_CLAIMED, raw_page_private_ptr(page));
 		return;
 	}
 
@@ -1279,9 +1283,9 @@ static void z3fold_free(struct z3fold_pool *pool, unsigned long handle)
 		return;
 	}
 	if (unlikely(PageIsolated(page)) ||
-	    test_and_set_bit(NEEDS_COMPACTING, &page->private)) {
+	    test_and_set_bit(NEEDS_COMPACTING, raw_page_private_ptr(page))) {
 		put_z3fold_header(zhdr);
-		clear_bit(PAGE_CLAIMED, &page->private);
+		clear_bit(PAGE_CLAIMED, raw_page_private_ptr(page));
 		return;
 	}
 	if (zhdr->cpu < 0 || !cpu_online(zhdr->cpu)) {
@@ -1290,12 +1294,12 @@ static void z3fold_free(struct z3fold_pool *pool, unsigned long handle)
 		spin_unlock(&pool->lock);
 		zhdr->cpu = -1;
 		kref_get(&zhdr->refcount);
-		clear_bit(PAGE_CLAIMED, &page->private);
+		clear_bit(PAGE_CLAIMED, raw_page_private_ptr(page));
 		do_compact_page(zhdr, true);
 		return;
 	}
 	kref_get(&zhdr->refcount);
-	clear_bit(PAGE_CLAIMED, &page->private);
+	clear_bit(PAGE_CLAIMED, raw_page_private_ptr(page));
 	queue_work_on(zhdr->cpu, pool->compact_wq, &zhdr->work);
 	put_z3fold_header(zhdr);
 }
@@ -1360,27 +1364,32 @@ static int z3fold_reclaim_page(struct z3fold_pool *pool, unsigned int retries)
 			/* this bit could have been set by free, in which case
 			 * we pass over to the next page in the pool.
 			 */
-			if (test_and_set_bit(PAGE_CLAIMED, &page->private)) {
+			if (test_and_set_bit(PAGE_CLAIMED,
+					     raw_page_private_ptr(page))) {
 				page = NULL;
 				continue;
 			}
 
 			if (unlikely(PageIsolated(page))) {
-				clear_bit(PAGE_CLAIMED, &page->private);
+				clear_bit(PAGE_CLAIMED,
+					  raw_page_private_ptr(page));
 				page = NULL;
 				continue;
 			}
 			zhdr = page_address(page);
-			if (test_bit(PAGE_HEADLESS, &page->private))
+			if (test_bit(PAGE_HEADLESS,
+				     raw_page_private_ptr(page)))
 				break;
 
 			if (!z3fold_page_trylock(zhdr)) {
-				clear_bit(PAGE_CLAIMED, &page->private);
+				clear_bit(PAGE_CLAIMED,
+					  raw_page_private_ptr(page));
 				zhdr = NULL;
 				continue; /* can't evict at this point */
 			}
 			if (zhdr->foreign_handles) {
-				clear_bit(PAGE_CLAIMED, &page->private);
+				clear_bit(PAGE_CLAIMED,
+					  raw_page_private_ptr(page));
 				z3fold_page_unlock(zhdr);
 				zhdr = NULL;
 				continue; /* can't evict such page */
@@ -1397,7 +1406,7 @@ static int z3fold_reclaim_page(struct z3fold_pool *pool, unsigned int retries)
 		list_del_init(&page->lru);
 		spin_unlock(&pool->lock);
 
-		if (!test_bit(PAGE_HEADLESS, &page->private)) {
+		if (!test_bit(PAGE_HEADLESS, raw_page_private_ptr(page))) {
 			/*
 			 * We need encode the handles before unlocking, and
 			 * use our local slots structure because z3fold_free
@@ -1442,7 +1451,7 @@ static int z3fold_reclaim_page(struct z3fold_pool *pool, unsigned int retries)
 			free_handle(last_handle);
 		}
 next:
-		if (test_bit(PAGE_HEADLESS, &page->private)) {
+		if (test_bit(PAGE_HEADLESS, raw_page_private_ptr(page))) {
 			if (ret == 0) {
 				free_z3fold_page(page, true);
 				atomic64_dec(&pool->pages_nr);
@@ -1451,7 +1460,7 @@ static int z3fold_reclaim_page(struct z3fold_pool *pool, unsigned int retries)
 			spin_lock(&pool->lock);
 			list_add(&page->lru, &pool->lru);
 			spin_unlock(&pool->lock);
-			clear_bit(PAGE_CLAIMED, &page->private);
+			clear_bit(PAGE_CLAIMED, raw_page_private_ptr(page));
 		} else {
 			z3fold_page_lock(zhdr);
 			if (kref_put(&zhdr->refcount,
@@ -1468,7 +1477,7 @@ static int z3fold_reclaim_page(struct z3fold_pool *pool, unsigned int retries)
 			list_add(&page->lru, &pool->lru);
 			spin_unlock(&pool->lock);
 			z3fold_page_unlock(zhdr);
-			clear_bit(PAGE_CLAIMED, &page->private);
+			clear_bit(PAGE_CLAIMED, raw_page_private_ptr(page));
 		}
 
 		/* We started off locked to we need to lock the pool back */
@@ -1499,7 +1508,7 @@ static void *z3fold_map(struct z3fold_pool *pool, unsigned long handle)
 	addr = zhdr;
 	page = virt_to_page(zhdr);
 
-	if (test_bit(PAGE_HEADLESS, &page->private))
+	if (test_bit(PAGE_HEADLESS, raw_page_private_ptr(page)))
 		goto out;
 
 	buddy = handle_to_buddy(handle);
@@ -1509,7 +1518,7 @@ static void *z3fold_map(struct z3fold_pool *pool, unsigned long handle)
 		break;
 	case MIDDLE:
 		addr += zhdr->start_middle << CHUNK_SHIFT;
-		set_bit(MIDDLE_CHUNK_MAPPED, &page->private);
+		set_bit(MIDDLE_CHUNK_MAPPED, raw_page_private_ptr(page));
 		break;
 	case LAST:
 		addr += PAGE_SIZE - (handle_to_chunks(handle) << CHUNK_SHIFT);
@@ -1542,12 +1551,12 @@ static void z3fold_unmap(struct z3fold_pool *pool, unsigned long handle)
 	zhdr = get_z3fold_header(handle);
 	page = virt_to_page(zhdr);
 
-	if (test_bit(PAGE_HEADLESS, &page->private))
+	if (test_bit(PAGE_HEADLESS, raw_page_private_ptr(page)))
 		return;
 
 	buddy = handle_to_buddy(handle);
 	if (buddy == MIDDLE)
-		clear_bit(MIDDLE_CHUNK_MAPPED, &page->private);
+		clear_bit(MIDDLE_CHUNK_MAPPED, raw_page_private_ptr(page));
 	zhdr->mapped_count--;
 	put_z3fold_header(zhdr);
 }
@@ -1571,14 +1580,14 @@ static bool z3fold_page_isolate(struct page *page, isolate_mode_t mode)
 	VM_BUG_ON_PAGE(!PageMovable(page), page);
 	VM_BUG_ON_PAGE(PageIsolated(page), page);
 
-	if (test_bit(PAGE_HEADLESS, &page->private) ||
-	    test_bit(PAGE_CLAIMED, &page->private))
+	if (test_bit(PAGE_HEADLESS, raw_page_private_ptr(page)) ||
+	    test_bit(PAGE_CLAIMED, raw_page_private_ptr(page)))
 		return false;
 
 	zhdr = page_address(page);
 	z3fold_page_lock(zhdr);
-	if (test_bit(NEEDS_COMPACTING, &page->private) ||
-	    test_bit(PAGE_STALE, &page->private))
+	if (test_bit(NEEDS_COMPACTING, raw_page_private_ptr(page)) ||
+	    test_bit(PAGE_STALE, raw_page_private_ptr(page)))
 		goto out;
 
 	if (zhdr->mapped_count != 0 || zhdr->foreign_handles != 0)
@@ -1628,8 +1637,8 @@ static int z3fold_page_migrate(struct address_space *mapping, struct page *newpa
 	}
 	new_zhdr = page_address(newpage);
 	memcpy(new_zhdr, zhdr, PAGE_SIZE);
-	newpage->private = page->private;
-	page->private = 0;
+	raw_page_private_set(newpage, raw_page_private(page));
+	raw_page_private_set(page, 0);
 	z3fold_page_unlock(zhdr);
 	spin_lock_init(&new_zhdr->page_lock);
 	INIT_WORK(&new_zhdr->work, compact_page_work);
@@ -1650,7 +1659,7 @@ static int z3fold_page_migrate(struct address_space *mapping, struct page *newpa
 		encode_handle(new_zhdr, LAST);
 	if (new_zhdr->middle_chunks)
 		encode_handle(new_zhdr, MIDDLE);
-	set_bit(NEEDS_COMPACTING, &newpage->private);
+	set_bit(NEEDS_COMPACTING, raw_page_private_ptr(newpage));
 	new_zhdr->cpu = smp_processor_id();
 	spin_lock(&pool->lock);
 	list_add(&newpage->lru, &pool->lru);
-- 
2.26.2

