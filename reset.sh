#!/bin/sh
SELF=`readlink -f $0`
BASE=`dirname $SELF`

if [ "`pwd`" = "$BASE" ] ; then
    echo "WRONG DIRECTORY !"
    exit
fi

# 5.7rc7 rebase
git reset --hard 9cb1fd0efd195590b828b9b865421ad345a4a145
