From f8ee19b0e20a5493ab55d27230f3b3fa2f90f933 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?J=C3=A9r=C3=B4me=20Glisse?= <jglisse@redhat.com>
Date: Fri, 29 May 2020 22:11:08 -0400
Subject: [PATCH] fs/nfs: use new fs_page_private*() helpers
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

New page->private helpers differentiate the type of page ie anonymous or
file back page from any other use of page (classified as raw page). This
to clarify what type of page we are expected to see and also get extra
necessary information for anonymous or file back page.

Signed-off-by: Jérôme Glisse <jglisse@redhat.com>
Cc: "J. Bruce Fields" <bfields@fieldses.org>
Cc: Chuck Lever <chuck.lever@oracle.com>
Cc: Trond Myklebust <trond.myklebust@hammerspace.com>
Cc: Anna Schumaker <anna.schumaker@netapp.com>
Cc: linux-nfs@vger.kernel.org
Cc: linux-fsdevel@vger.kernel.org
Cc: linux-mm@kvack.org
Cc: Alexander Viro <viro@zeniv.linux.org.uk>
Cc: Tejun Heo <tj@kernel.org>
Cc: Jan Kara <jack@suse.cz>
Cc: Josef Bacik <jbacik@fb.com>
Cc: Andrew Morton <akpm@linux-foundation.org>
---
 fs/nfs/file.c  | 18 ++++++++++++------
 fs/nfs/read.c  |  2 +-
 fs/nfs/write.c | 28 +++++++++++++++++-----------
 3 files changed, 30 insertions(+), 18 deletions(-)

diff --git a/fs/nfs/file.c b/fs/nfs/file.c
index 742a514067b8..a8b12cf760df 100644
--- a/fs/nfs/file.c
+++ b/fs/nfs/file.c
@@ -286,14 +286,16 @@ static bool nfs_full_page_write(struct page *page, loff_t pos, unsigned int len)
 	return !pglen || (end >= pglen && !offset);
 }
 
-static bool nfs_want_read_modify_write(struct file *file, struct page *page,
-			loff_t pos, unsigned int len)
+static bool nfs_want_read_modify_write(struct file *file,
+				       struct address_space *mapping,
+				       struct page *page, loff_t pos,
+				       unsigned int len)
 {
 	/*
 	 * Up-to-date pages, those with ongoing or full-page write
 	 * don't need read/modify/write
 	 */
-	if (PageUptodate(page) || PagePrivate(page) ||
+	if (PageUptodate(page) || FSPagePrivate(mapping, page) ||
 	    nfs_full_page_write(page, pos, len))
 		return false;
 
@@ -336,7 +338,7 @@ static int nfs_write_begin(struct file *file, struct address_space *mapping,
 		unlock_page(page);
 		put_page(page);
 	} else if (!once_thru &&
-		   nfs_want_read_modify_write(file, page, pos, len)) {
+		   nfs_want_read_modify_write(file, mapping, page, pos, len)) {
 		once_thru = 1;
 		ret = nfs_readpage(file, page);
 		put_page(page);
@@ -424,10 +426,14 @@ static void nfs_invalidate_page(struct page *page, unsigned int offset,
  */
 static int nfs_release_page(struct page *page, gfp_t gfp)
 {
+	struct address_space *mapping;
+
+	mapping = page->mapping;
+
 	dfprintk(PAGECACHE, "NFS: release_page(%p)\n", page);
 
 	/* If PagePrivate() is set, then the page is not freeable */
-	if (PagePrivate(page))
+	if (FSPagePrivate(mapping, page))
 		return 0;
 	return nfs_fscache_release_page(page, gfp);
 }
@@ -457,7 +463,7 @@ static void nfs_check_dirty_writeback(struct page *page,
 	 * inode is not being committed, it's not going to be cleaned in the
 	 * near future so treat it as dirty
 	 */
-	if (PagePrivate(page))
+	if (FSPagePrivate(mapping, page))
 		*dirty = true;
 }
 
diff --git a/fs/nfs/read.c b/fs/nfs/read.c
index 3b72f0f65234..2208df0d44a0 100644
--- a/fs/nfs/read.c
+++ b/fs/nfs/read.c
@@ -107,7 +107,7 @@ static void nfs_readpage_release(struct nfs_page *req, int error)
 
 		if (PageUptodate(page))
 			nfs_readpage_to_fscache(inode, page, 0);
-		else if (!PageError(page) && !PagePrivate(page))
+		else if (!PageError(page) && !FSPagePrivate(mapping, page))
 			generic_error_remove_page(mapping, page);
 		unlock_page(page);
 	}
diff --git a/fs/nfs/write.c b/fs/nfs/write.c
index ec638616eb61..7b7c6a0bb1fd 100644
--- a/fs/nfs/write.c
+++ b/fs/nfs/write.c
@@ -175,11 +175,11 @@ nfs_cancel_remove_inode(struct nfs_page *req, struct inode *inode)
 }
 
 static struct nfs_page *
-nfs_page_private_request(struct page *page)
+nfs_page_private_request(struct address_space *mapping, struct page *page)
 {
-	if (!PagePrivate(page))
+	if (!FSPagePrivate(mapping, page))
 		return NULL;
-	return (struct nfs_page *)page_private(page);
+	return fs_page_private(mapping, page);
 }
 
 /*
@@ -195,10 +195,10 @@ nfs_page_find_private_request(struct page *page)
 	struct address_space *mapping = page_file_mapping(page);
 	struct nfs_page *req;
 
-	if (!PagePrivate(page))
+	if (!FSPagePrivate(mapping, page))
 		return NULL;
 	spin_lock(&mapping->private_lock);
-	req = nfs_page_private_request(page);
+	req = nfs_page_private_request(mapping, page);
 	if (req) {
 		WARN_ON_ONCE(req->wb_head != req);
 		kref_get(&req->wb_kref);
@@ -264,7 +264,7 @@ static struct nfs_page *nfs_find_and_lock_page_request(struct page *page)
 			return ERR_PTR(ret);
 		}
 		/* Ensure that nobody removed the request before we locked it */
-		if (head == nfs_page_private_request(page))
+		if (head == nfs_page_private_request(inode->i_mapping, page))
 			break;
 		if (PageSwapCache(page))
 			break;
@@ -763,8 +763,8 @@ static void nfs_inode_add_request(struct inode *inode, struct nfs_page *req)
 		inode_inc_iversion_raw(inode);
 	if (likely(!PageSwapCache(req->wb_page))) {
 		set_bit(PG_MAPPED, &req->wb_flags);
-		SetPagePrivate(req->wb_page);
-		set_page_private(req->wb_page, (unsigned long)req);
+		FSPagePrivateSet(mapping, req->wb_page);
+		fs_page_private_set(mapping, req->wb_page, req);
 	}
 	spin_unlock(&mapping->private_lock);
 	atomic_long_inc(&nfsi->nrequests);
@@ -791,8 +791,8 @@ static void nfs_inode_remove_request(struct nfs_page *req)
 
 		spin_lock(&mapping->private_lock);
 		if (likely(head->wb_page && !PageSwapCache(head->wb_page))) {
-			set_page_private(head->wb_page, 0);
-			ClearPagePrivate(head->wb_page);
+			fs_page_private_set(mapping, head->wb_page, NULL);
+			FSPagePrivateClear(mapping, head->wb_page);
 			clear_bit(PG_MAPPED, &head->wb_flags);
 		}
 		spin_unlock(&mapping->private_lock);
@@ -2089,7 +2089,7 @@ int nfs_wb_page(struct inode *inode, struct page *page, loff_t range_start)
 			continue;
 		}
 		ret = 0;
-		if (!PagePrivate(page))
+		if (!FSPagePrivate(inode->i_mapping, page))
 			break;
 		ret = nfs_commit_inode(inode, FLUSH_SYNC);
 		if (ret < 0)
@@ -2104,6 +2104,12 @@ int nfs_wb_page(struct inode *inode, struct page *page, loff_t range_start)
 int nfs_migrate_page(struct address_space *mapping, struct page *newpage,
 		struct page *page, enum migrate_mode mode)
 {
+	/*
+	 * FIXME ascertain that we can only get regular file page ie page
+	 * with page->mapping == mapping (ignoring truncate case here). So
+	 * that it is safe to use the raw_page_*() helpers.
+	 */
+
 	/*
 	 * If PagePrivate is set, then the page is currently associated with
 	 * an in-progress read or write request. Don't try to migrate it.
-- 
2.26.2

