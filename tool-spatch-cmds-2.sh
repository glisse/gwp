#!/bin/sh
spatch_file=$1
field=$2

git grep -l "\->$field" -- '*.[ch]' | uniq | while read file ; do
    echo $file
    spatch --in-place --sp-file $spatch_file $file
done
