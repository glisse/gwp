#!/bin/sh
SELF=`readlink -f $0`
BASE=`dirname $SELF`
source $BASE/tool-helpers.sh
if ! git diff-index --quiet HEAD; then
    echo "Git tree is not clean, abort !"
    exit -1
fi

patch pxa--p02-mm-rename-__page_file_index-to-swapfile_page_index.patch
patch pxa--p02-mm-fs-add-helpers-for-accessing-page-index-field.patch
patch pxa--p02-mm-huge-use-raw_page_index.patch
patch pxa--p02-fs-f2fs-cleanup-page-index-usage.patch
patch pxa--p02-fs-nfs-cleanup-page-index-usage.patch
patch pxa--p02-fs-btrfs-cleanup-page-index-usage.patch
patch pxa--p02-fs-ceph-use-fs_page_index-instead-of-page_index.patch
patch pxa--p02-fs-nilfs2-cleanup-page-index-usage.patch
patch pxa--p02-fs-erofs-use-appropriate-fs_page_index-helper.patch
patch pxa--p02-fs-afs-use-appropriate-fs_page_index-helper.patch
patch pxa--p02-fs-fscache-use-appropriate-fs_page_index-helper.patch
patch pxa--p02-fs-readpage-use-raw_page_index-in-for-new-fs-page.patch
patch pxa--p02-mm-filemap-use-fs_page_index-helpers.patch
patch pxa--p02-mm-fs-do-not-use-page_index-within-__set_page_dirty.patch
patch pxa--p02-mm-fs-truncate-use-fs_page_index.patch
patch pxa--p02-mm-ksm-use-proper-helpers-to-access-page-index.patch
patch pxa--p02-mm-pfmemalloc-use-raw_page_index-helpers.patch
patch pxa--p02-mm-use-raw_page_index-helpers.patch
patch pxa--p02-mm-use-anon_page_index-where-appropriate.patch
patch pxa--p02-mm-migrate-use-fs_page_index-or-raw_page_mapping-hel.patch
patch pxa--p02-mm-huge-use-raw_page_mapping-helpers.patch
patch pxa--p02-mm-zsmalloc-use-raw_page_index-helpers.patch
patch pxa--p02-mm-__set_page_dirty_nobuffers-use-fs_or_swapfile_pag.patch
patch pxa--p02-mm-__test_set_page_writeback-test_clear_page_writeba.patch
patch pxa--p02-mm-stop-using-page_to_pgoff-everywhere-it-is-possibl.patch
patch pxa--p02-mm-hugetlbfs-use-hugetlbfs_page_index.patch
patch pxa--p02-mm-page-io-use-swapfile_page_index-in-swap-io-code.patch
patch pxa--p02-mm-fs-use-fs_page_index-in-page_mkwrite_check_trunca.patch
patch pxa--p02-futex-use-replace-basepage_index-with-fs_page_index.patch
patch pxa--p02-drivers-block-use-raw_page_index-helpers.patch
patch pxa--p02-drivers-md-use-raw_page_index-helpers.patch
patch pxa--p02-net-sun-use-raw_page_index-helpers.patch
patch pxa--p02-arch-xtensa-use-raw_page_index-helpers.patch
patch pxa--p02-arch-s390-use-raw_page_index-helpers.patch
patch pxa--p02-arch-parisc-use-fs_page_index-helpers.patch
patch pxa--p02-arch-nios2-use-fs_page_index-helpers.patch
patch pxa--p02-arch-arm-use-fs_page_index.patch
patch pxa--p02-video-fbdev-core-use-fs_page_index-helpers.patch
patch pxa--p02-video-fbdev-update-a-bunch-of-fbdev-driver-to-use-ra.patch
patch pxa--p02-drm-ttm-use-proper-helpers-to-access-page-index.patch
patch pxa--p02-intel_th-use-fs_page_index-helpers.patch
patch pxa--p02-drivers-dax-use-fs_page_index-helpers.patch
patch pxa--p02-perf-core-use-fs_page_index-helpers.patch
spatch tool-spatch-cmds-1.sh pxa--p02-use-fs_page_index fs mm
patch pxa--p02-mm-fs-remove-old-page-index-helpers.patch
patch pxa--p02-mm-hide-struct-page.index-field.patch
