From 59117c6d8d8fa8c4eb7c5b1d640121cf8474ee51 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?J=C3=A9r=C3=B4me=20Glisse?= <jglisse@redhat.com>
Date: Sat, 30 May 2020 15:43:37 -0400
Subject: [PATCH] mm/hugetlbfs: use raw_page*() helpers
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Hugetlbfs page will not be eligible to page exclusive access (PXA), at
least for now. Thus they should be considered as raw page reflect that
by using raw_page* helpers.

Signed-off-by: Jérôme Glisse <jglisse@redhat.com>
Cc: Mike Kravetz <mike.kravetz@oracle.com>
Cc: linux-fsdevel@vger.kernel.org
Cc: linux-mm@kvack.org
Cc: Alexander Viro <viro@zeniv.linux.org.uk>
Cc: Tejun Heo <tj@kernel.org>
Cc: Jan Kara <jack@suse.cz>
Cc: Josef Bacik <jbacik@fb.com>
Cc: Andrew Morton <akpm@linux-foundation.org>
---
 fs/hugetlbfs/inode.c           | 12 +++++++++---
 include/linux/hugetlb_cgroup.h |  8 ++++----
 mm/hugetlb.c                   |  6 +++---
 3 files changed, 16 insertions(+), 10 deletions(-)

diff --git a/fs/hugetlbfs/inode.c b/fs/hugetlbfs/inode.c
index d10407e83f6b..20dd68fef8e1 100644
--- a/fs/hugetlbfs/inode.c
+++ b/fs/hugetlbfs/inode.c
@@ -910,6 +910,12 @@ static int hugetlbfs_migrate_page(struct address_space *mapping,
 {
 	int rc;
 
+	/*
+	 * FIXME ascertain that we can only get regular file page ie page
+	 * with page->mapping == mapping (ignoring truncate case here). So
+	 * that it is safe to use the raw_page_*() helpers.
+	 */
+
 	rc = migrate_huge_page_move_mapping(mapping, newpage, page);
 	if (rc != MIGRATEPAGE_SUCCESS)
 		return rc;
@@ -920,9 +926,9 @@ static int hugetlbfs_migrate_page(struct address_space *mapping,
 	 * hugetlb pages and can not be set here as only page_huge_active
 	 * pages can be migrated.
 	 */
-	if (page_private(page)) {
-		set_page_private(newpage, page_private(page));
-		set_page_private(page, 0);
+	if (raw_page_private(page)) {
+		raw_page_private_set(newpage, raw_page_private(page));
+		raw_page_private_set(page, 0);
 	}
 
 	if (mode != MIGRATE_SYNC_NO_COPY)
diff --git a/include/linux/hugetlb_cgroup.h b/include/linux/hugetlb_cgroup.h
index 2ad6e92f124a..e4f39d48d351 100644
--- a/include/linux/hugetlb_cgroup.h
+++ b/include/linux/hugetlb_cgroup.h
@@ -66,9 +66,9 @@ __hugetlb_cgroup_from_page(struct page *page, bool rsvd)
 	if (compound_order(page) < HUGETLB_CGROUP_MIN_ORDER)
 		return NULL;
 	if (rsvd)
-		return (struct hugetlb_cgroup *)page[3].private;
+		return (void *)raw_page_private(&page[3]);
 	else
-		return (struct hugetlb_cgroup *)page[2].private;
+		return (void *)raw_page_private(&page[2]);
 }
 
 static inline struct hugetlb_cgroup *hugetlb_cgroup_from_page(struct page *page)
@@ -90,9 +90,9 @@ static inline int __set_hugetlb_cgroup(struct page *page,
 	if (compound_order(page) < HUGETLB_CGROUP_MIN_ORDER)
 		return -1;
 	if (rsvd)
-		page[3].private = (unsigned long)h_cg;
+		raw_page_private_set(&page[3], (unsigned long)h_cg);
 	else
-		page[2].private = (unsigned long)h_cg;
+		raw_page_private_set(&page[2], (unsigned long)h_cg);
 	return 0;
 }
 
diff --git a/mm/hugetlb.c b/mm/hugetlb.c
index ffff924bd802..e89c3d08c199 100644
--- a/mm/hugetlb.c
+++ b/mm/hugetlb.c
@@ -1388,13 +1388,13 @@ static void __free_huge_page(struct page *page)
 	struct hstate *h = page_hstate(page);
 	int nid = page_to_nid(page);
 	struct hugepage_subpool *spool =
-		(struct hugepage_subpool *)page_private(page);
+		(struct hugepage_subpool *)raw_page_private(page);
 	bool restore_reserve;
 
 	VM_BUG_ON_PAGE(page_count(page), page);
 	VM_BUG_ON_PAGE(page_mapcount(page), page);
 
-	set_page_private(page, 0);
+	raw_page_private_set(page, 0);
 	page->mapping = NULL;
 	restore_reserve = PagePrivate(page);
 	ClearPagePrivate(page);
@@ -2427,7 +2427,7 @@ struct page *alloc_huge_page(struct vm_area_struct *vma,
 
 	spin_unlock(&hugetlb_lock);
 
-	set_page_private(page, (unsigned long)spool);
+	raw_page_private_set(page, (unsigned long)spool);
 
 	map_commit = vma_commit_reservation(h, vma, addr);
 	if (unlikely(map_chg > map_commit)) {
-- 
2.26.2

