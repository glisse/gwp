From 06b0cc6f862e9d8dae8a844dfd083d4ee91eeb29 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?J=C3=A9r=C3=B4me=20Glisse?= <jglisse@redhat.com>
Date: Fri, 29 May 2020 12:59:16 -0400
Subject: [PATCH] fs/f2fs: use new fs_page_private*() or raw_page_private*()
 helpers
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

New page->private helpers differentiate the type of page ie anonymous or
file back page from any other use of page (classified as raw page). This
to clarify what type of page we are expected to see and also get extra
necessary information for anonymous or file back page.

Signed-off-by: Jérôme Glisse <jglisse@redhat.com>
Cc: Jaegeuk Kim <jaegeuk@kernel.org>
Cc: Chao Yu <yuchao0@huawei.com>
Cc: linux-f2fs-devel@lists.sourceforge.net
Cc: linux-fsdevel@vger.kernel.org
Cc: linux-mm@kvack.org
Cc: Alexander Viro <viro@zeniv.linux.org.uk>
Cc: Tejun Heo <tj@kernel.org>
Cc: Jan Kara <jack@suse.cz>
Cc: Josef Bacik <jbacik@fb.com>
Cc: Andrew Morton <akpm@linux-foundation.org>
---
 fs/f2fs/compress.c | 18 ++++++++++--------
 fs/f2fs/data.c     | 18 ++++++++++++------
 fs/f2fs/f2fs.h     | 23 +++++++++++++----------
 fs/f2fs/trace.c    |  4 ++--
 4 files changed, 37 insertions(+), 26 deletions(-)

diff --git a/fs/f2fs/compress.c b/fs/f2fs/compress.c
index 40b686ce5dd5..ae4330b7fa3c 100644
--- a/fs/f2fs/compress.c
+++ b/fs/f2fs/compress.c
@@ -45,12 +45,13 @@ bool f2fs_is_compressed_page(struct page *page)
 {
 	if (!PagePrivate(page))
 		return false;
-	if (!page_private(page))
+	if (!raw_page_private(page))
 		return false;
 	if (IS_ATOMIC_WRITTEN_PAGE(page) || IS_DUMMY_WRITTEN_PAGE(page))
 		return false;
 	f2fs_bug_on(F2FS_M_SB(page->mapping),
-		*((u32 *)page_private(page)) != F2FS_COMPRESSED_PAGE_MAGIC);
+		    *((u32*)raw_page_private(page)) !=
+		    F2FS_COMPRESSED_PAGE_MAGIC);
 	return true;
 }
 
@@ -58,7 +59,7 @@ static void f2fs_set_compressed_page(struct page *page,
 		struct inode *inode, pgoff_t index, void *data)
 {
 	SetPagePrivate(page);
-	set_page_private(page, (unsigned long)data);
+	raw_page_private_set(page, (unsigned long)data);
 
 	/* i_crypto_info and iv index */
 	page->mapping = inode->i_mapping;
@@ -67,7 +68,7 @@ static void f2fs_set_compressed_page(struct page *page,
 
 static void f2fs_put_compressed_page(struct page *page)
 {
-	set_page_private(page, (unsigned long)NULL);
+	raw_page_private_set(page, (unsigned long)NULL);
 	ClearPagePrivate(page);
 	page->mapping = NULL;
 	unlock_page(page);
@@ -128,7 +129,7 @@ static void f2fs_put_rpages_wbc(struct compress_ctx *cc,
 
 struct page *f2fs_compress_control_page(struct page *page)
 {
-	return ((struct compress_io_ctx *)page_private(page))->rpages[0];
+	return ((struct compress_io_ctx *)raw_page_private(page))->rpages[0];
 }
 
 int f2fs_init_compress_ctx(struct compress_ctx *cc)
@@ -598,7 +599,7 @@ static int f2fs_compress_pages(struct compress_ctx *cc)
 void f2fs_decompress_pages(struct bio *bio, struct page *page, bool verity)
 {
 	struct decompress_io_ctx *dic =
-			(struct decompress_io_ctx *)page_private(page);
+			(struct decompress_io_ctx *)raw_page_private(page);
 	struct f2fs_sb_info *sbi = F2FS_I_SB(dic->inode);
 	struct f2fs_inode_info *fi= F2FS_I(dic->inode);
 	const struct f2fs_compress_ops *cops =
@@ -1129,10 +1130,11 @@ static int f2fs_write_compressed_pages(struct compress_ctx *cc,
 void f2fs_compress_write_end_io(struct bio *bio, struct page *page)
 {
 	struct f2fs_sb_info *sbi = bio->bi_private;
-	struct compress_io_ctx *cic =
-			(struct compress_io_ctx *)page_private(page);
+	struct compress_io_ctx *cic;
 	int i;
 
+	cic = (struct compress_io_ctx *)raw_page_private(page);
+
 	if (unlikely(bio->bi_status))
 		mapping_set_error(cic->inode->i_mapping, -EIO);
 
diff --git a/fs/f2fs/data.c b/fs/f2fs/data.c
index 51a38d9b9b05..c027fa054b6d 100644
--- a/fs/f2fs/data.c
+++ b/fs/f2fs/data.c
@@ -193,10 +193,14 @@ static void f2fs_verify_bio(struct bio *bio)
 	struct bvec_iter_all iter_all;
 
 	bio_for_each_segment_all(bv, bio, iter_all) {
-		struct page *page = bv->bv_page;
+		struct address_space *mapping;
 		struct decompress_io_ctx *dic;
+		struct page *page;
 
-		dic = (struct decompress_io_ctx *)page_private(page);
+		page = bv->bv_page;
+		mapping = page->mapping;
+
+		dic = fs_page_private(mapping, page);
 
 		if (dic) {
 			if (refcount_dec_not_one(&dic->ref))
@@ -347,7 +351,7 @@ static void f2fs_write_end_io(struct bio *bio)
 		enum count_type type = WB_DATA_TYPE(page);
 
 		if (IS_DUMMY_WRITTEN_PAGE(page)) {
-			set_page_private(page, (unsigned long)NULL);
+			raw_page_private_set(page, 0);
 			ClearPagePrivate(page);
 			unlock_page(page);
 			mempool_free(page, sbi->write_io_dummy);
@@ -488,7 +492,8 @@ static inline void __submit_bio(struct f2fs_sb_info *sbi,
 
 			zero_user_segment(page, 0, PAGE_SIZE);
 			SetPagePrivate(page);
-			set_page_private(page, (unsigned long)DUMMY_WRITTEN_PAGE);
+			raw_page_private_set(page, (unsigned long)
+					     DUMMY_WRITTEN_PAGE);
 			lock_page(page);
 			if (bio_add_page(bio, page, PAGE_SIZE, 0) < PAGE_SIZE)
 				f2fs_bug_on(sbi, 1);
@@ -3673,8 +3678,9 @@ int f2fs_migrate_page(struct address_space *mapping,
 		get_page(newpage);
 	}
 
-	if (PagePrivate(page)) {
-		f2fs_set_page_private(newpage, page_private(page));
+	if (FSPagePrivate(mapping, page)) {
+		f2fs_set_page_private(newpage, (long)
+				      fs_page_private(mapping, page));
 		f2fs_clear_page_private(page);
 	}
 
diff --git a/fs/f2fs/f2fs.h b/fs/f2fs/f2fs.h
index ba470d5687fe..2d4dd070038f 100644
--- a/fs/f2fs/f2fs.h
+++ b/fs/f2fs/f2fs.h
@@ -1253,9 +1253,9 @@ enum fsync_mode {
 #define DUMMY_WRITTEN_PAGE		((unsigned long)-2)
 
 #define IS_ATOMIC_WRITTEN_PAGE(page)			\
-		(page_private(page) == (unsigned long)ATOMIC_WRITTEN_PAGE)
+		(raw_page_private(page) == (unsigned long)ATOMIC_WRITTEN_PAGE)
 #define IS_DUMMY_WRITTEN_PAGE(page)			\
-		(page_private(page) == (unsigned long)DUMMY_WRITTEN_PAGE)
+		(raw_page_private(page) == (unsigned long)DUMMY_WRITTEN_PAGE)
 
 #ifdef CONFIG_FS_ENCRYPTION
 #define DUMMY_ENCRYPTION_ENABLED(sbi) \
@@ -3045,24 +3045,27 @@ static inline bool __is_valid_data_blkaddr(block_t blkaddr)
 	return true;
 }
 
-static inline void f2fs_set_page_private(struct page *page,
-						unsigned long data)
+static inline void f2fs_set_page_private(struct page *page, unsigned long data)
 {
-	if (PagePrivate(page))
+	struct address_space *mapping = page->mapping;
+
+	if (FSPagePrivate(mapping, page))
 		return;
 
 	get_page(page);
-	SetPagePrivate(page);
-	set_page_private(page, data);
+	FSPagePrivateSet(mapping, page);
+	fs_page_private_set(mapping, page, (void *)data);
 }
 
 static inline void f2fs_clear_page_private(struct page *page)
 {
-	if (!PagePrivate(page))
+	struct address_space *mapping = page->mapping;
+
+	if (!FSPagePrivate(mapping, page))
 		return;
 
-	set_page_private(page, 0);
-	ClearPagePrivate(page);
+	fs_page_private_set(mapping, page, 0);
+	FSPagePrivate(mapping, page);
 	f2fs_put_page(page, 0);
 }
 
diff --git a/fs/f2fs/trace.c b/fs/f2fs/trace.c
index d0ab533a9ce8..f2c82c6d3342 100644
--- a/fs/f2fs/trace.c
+++ b/fs/f2fs/trace.c
@@ -56,7 +56,7 @@ void f2fs_trace_pid(struct page *page)
 	pid_t pid = task_pid_nr(current);
 	void *p;
 
-	set_page_private(page, (unsigned long)pid);
+	fs_page_private_set(inode->i_mapping, page, (void *)((long)pid));
 
 retry:
 	if (radix_tree_preload(GFP_NOFS))
@@ -96,7 +96,7 @@ void f2fs_trace_ios(struct f2fs_io_info *fio, int flush)
 	}
 
 	inode = fio->page->mapping->host;
-	pid = page_private(fio->page);
+	pid = (pid_t)((long)fs_page_private(inode->i_mapping, fio->page));
 
 	major = MAJOR(inode->i_sb->s_dev);
 	minor = MINOR(inode->i_sb->s_dev);
-- 
2.26.2

