#!/bin/bash
self=`realpath -s $0`
base=`dirname $self`
funcname=$1
shift
args=$@

spatch --include-headers --sp-file $base/tool-find-funcs-calling-a-func.spatch \
       -D fn=$funcname $args 2> /dev/null
