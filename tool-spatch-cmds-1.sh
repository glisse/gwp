#!/bin/sh
spatch_file=$1
dir=$2

while [ -n "${dir}" ]; do
    echo spatch --dir $dir --include-headers --in-place --sp-file $spatch_file
    spatch --dir $dir --include-headers --in-place --sp-file $spatch_file

    # next directory
    shift 1
    dir=$2
done
