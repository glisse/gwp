#!/bin/sh
SELF=`readlink -f $0`
BASE=`dirname $SELF`
source $BASE/tool-helpers.sh
if ! git diff-index --quiet HEAD; then
    echo "Git tree is not clean, abort !"
    exit -1
fi

patch pxa--p01-fs-define-filler_t-as-a-function-pointer-type.patch
patch pxa--p01-fs-cleancache-remove-cleancache_fs_enabled.patch
patch pxa--p01-fs-btrfs-figure-out-the-mapping-of-the-page-outside-.patch
patch pxa--p01-fs-erofs-figure-out-the-mapping-of-the-page-outside-.patch
patch pxa--p01-fs-ext4-figure-out-the-mapping-of-the-page-outside-t.patch
patch pxa--p01-fs-f2fs-figure-out-the-mapping-of-the-page-outside-t.patch
patch pxa--p01-mm-filemap-figure-out-the-mapping-of-the-page-outsid.patch
patch pxa--p01-fs-writeback-figure-out-the-mapping-of-the-page-outs.patch
patch pxa--p01-xarray-add-XA_STATE_INIT-helper.patch
patch pxa--p01-fs-btrfs-add-an-helper-to-get-mapping-of-page-in-an-.patch
patch pxa--p01-fs-fuse-keep-mapping-corresponding-to-page-around.patch
patch pxa--p01-mm-pxa-page-exclusive-access-add-header-file-for-all.patch
