// Find all function that call a given function and that do not have
// a mapping or an inode or a vma as one of its arguments

// Match function
@M1 exists@
identifier virtual.fn;
identifier caller;
position P;
@@
caller(...) {<+... fn@P(...) ...+>}

// What we need to ignore
@I1 exists@
identifier M1.caller, id;
@@
caller(..., struct address_space *id, ...) {...}

@I2 exists@
identifier M1.caller, id;
@@
caller(..., struct inode *id, ...) {...}

@I3 exists@
identifier M1.caller, id;
@@
caller(..., struct vm_area_struct *id, ...) {...}

@I4 exists@
identifier M1.caller, id;
type rtype;
@@
static rtype caller(...) {...}

@I5 exists@
identifier M1.caller, id;
@@
caller(..., struct extent_buffer *id, ...) {...}

@I6 exists@
identifier M1.caller, id;
@@
caller(...) {... struct compressed_bio *id; ...}

@I7 exists@
identifier M1.caller, id;
@@
caller(..., struct vm_fault *id, ...) {...}

@I8 exists@
identifier M1.caller, id;
@@
caller(..., struct buffer_head *id, ...) {...}

@I9 exists@
identifier M1.caller, id;
@@
caller(..., struct kiocb *id, ...) {...}


@script:python depends on M1 && !I1 && !I2 && !I3 && !I4 && !I5 && !I6 && !I7 && !I8 && !I9@
calling << virtual.fn;
caller << M1.caller;
P << M1.P;
@@
print(caller + " " + P[0].file + " " + calling + " global")

@script:python depends on M1 && !I1 && !I2 && !I3 && I4 && !I5 && !I6 && !I7 && !I8 && !I9@
calling << virtual.fn;
caller << M1.caller;
P << M1.P;
@@
print(caller + " " + P[0].file + " " + calling + " static")
