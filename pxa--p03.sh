#!/bin/sh
SELF=`readlink -f $0`
BASE=`dirname $SELF`
source $BASE/tool-helpers.sh
if ! git diff-index --quiet HEAD; then
    echo "Git tree is not clean, abort !"
    exit -1
fi

patch pxa--p03-mm-fs-add-helpers-for-accessing-page-private-field.patch
patch pxa--p03-arch-s390-use-raw_page_private_has-for-now.patch
patch pxa--p03-fs-iomap-use-raw_page_private_has-for-now.patch
patch pxa--p03-fs-ext4-manual-convertion-to-use-fs_page_private_has.patch
patch pxa--p03-fs-btrfs-use-raw_page_private_has-for-now.patch
patch pxa--p03-fs-trace-use-raw_page_private_has.patch
patch pxa--p03-mm-use-raw_page_private_has-for-now.patch
spatch tool-spatch-cmds-1.sh pxa--p03-use-fs_page_private_has fs mm
patch pxa--p03-arch-x86-use-new-raw_page_private-helpers.patch
patch pxa--p03-block-drbd-use-new-raw_page_private-helpers.patch
patch pxa--p03-fs-use-new-fs_page_private-or-raw_page_private-helpe.patch
patch pxa--p03-fs-jfs-use-new-fs_page_private-helpers.patch
patch pxa--p03-fs-orangefs-use-new-fs_page_private-helpers.patch
patch pxa--p03-fs-afs-use-new-fs_page_private-helpers.patch
patch pxa--p03-fs-f2fs-use-new-fs_page_private-or-raw_page_private-.patch
patch pxa--p03-fs-btrfs-use-new-fs_page_private-or-raw_page_private.patch
patch pxa--p03-fs-nfs-use-new-fs_page_private-helpers.patch
patch pxa--p03-fs-erofs-use-new-fs_page_private-or-raw_page_private.patch
patch pxa--p03-fs-9p-use-new-fs_page_private-helpers.patch
patch pxa--p03-fs-cifs-use-new-fs_page_private-helpers.patch
patch pxa--p03-fs-ubifs-use-new-fs_page_private-helpers.patch
patch pxa--p03-fs-ceph-use-new-fs_page_private-helpers.patch
patch pxa--p03-mm-balloon-use-raw_page_private.patch
patch pxa--p03-mm-migrate-use-raw_page_private.patch
patch pxa--p03-mm-memory_hoptlug-use-raw_page_private.patch
patch pxa--p03-mm-frontswap-use-raw_page_private.patch
patch pxa--p03-mm-zsmalloc-use-raw_page_private.patch
patch pxa--p03-mm-memcontrol-use-raw_page_private.patch
patch pxa--p03-mm-shmem-use-raw_page_private.patch
patch pxa--p03-mm-swap-use-raw_page_private.patch
patch pxa--p03-mm-compaction-use-raw_page_private.patch
patch pxa--p03-mm-for-free-page-use-raw_page_private-helpers.patch
patch pxa--p03-mm-thp-THP-use-as-anonymous-page-use-raw_page-helper.patch
patch pxa--p03-mm-hugetlbfs-use-raw_page-helpers.patch
patch pxa--p03-mm-z3fold-use-raw_page_private.patch
patch pxa--p03-kernel-kexec-use-raw_page_private.patch
patch pxa--p03-kernel-events-use-raw_page_private.patch
patch pxa--p03-kernel-relay-use-raw_page_private.patch
patch pxa--p03-xen-use-new-raw_page_private-helpers.patch
patch pxa--p03-firewire-use-new-raw_page_private-helpers.patch
patch pxa--p03-md-use-new-raw_page_private-helpers.patch
patch pxa--p03-net-core-use-new-raw_page_private-helpers.patch
patch pxa--p03-net-virtio-use-new-raw_page_private-helpers.patch
patch pxa--p03-block-blk-mq-use-raw_page_private.patch
patch pxa--p03-mm-hide-struct-page.private-field.patch
