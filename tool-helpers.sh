# common helpers

# -----------------------------------------------------------------------------

SELF=`readlink -f $0`
BASE=`dirname $SELF`
resume=$1
patchid=1

# -----------------------------------------------------------------------------

function spatch()
{
    spatch_cmds=$1
    spatch_file=$2
    shift 2

    # Make sure we do not get kill if git fails ...
    set +e
    trap null ERR

    if [ ! -z $resume ] ; then
        if [ $resume -gt $patchid ] ; then
            echo -e "[$patchid] [\e[92mOK\e[0m] $spatch_file !! SKIPPED !!"
            patchid=$((patchid + 1))
            return
        fi
    fi

    # Save current git HEAD SHA and use it to revert !
    save_sha=`git rev-parse HEAD`

    # The actual zemantic path
    $BASE/$spatch_cmds $BASE/$spatch_file.spatch $@ &> /dev/null
    if [ $? -ne 0 ]; then
        echo -e "[$patchid] [\e[91mEE\e[0m] $spatch_file"
        echo "cmd: $BASE/$spatch_cmds $BASE/$spatch_file.spatch" $@
        exit 1
    fi

    # Create our commit message by merging spatch and commit message
    awk "/%<---/{i++; print \$0; next}\
        i==1{i++;print system(\"cat $BASE/$spatch_cmds\") > /dev/null}\
        i==3{i++;print system(\"cat $BASE/$spatch_file.spatch\") > /dev/null} 1"\
        $BASE/$spatch_file.txt > /tmp/unicorn-commit
    if [ $? -ne 0 ]; then
        echo -e "[$patchid] [\e[91mEE\e[0m] $spatch_file (commit msg failed)"
        exit 1
    fi

    # Commit thing ...
    git commit --author="Jérôme Glisse <jglisse@redhat.com>" \
        -a -F /tmp/unicorn-commit &> /dev/null
    rm /tmp/unicorn-commit

    # On est bon là ! on est très bon ...
    echo -e "[$patchid] [\e[92mOK\e[0m] $spatch_file"

    patchid=$((patchid + 1))
}

# -----------------------------------------------------------------------------

function patch()
{
    if [ ! -z $resume ] ; then
        if [ $resume -gt $patchid ] ; then
            echo -e "[$patchid] [\e[92mOK\e[0m] $1 !! SKIPPED !!"
            patchid=$((patchid + 1))
            return
        fi
    fi

    # Make sure we do not get kill if git fails ...
    set +e
    trap null ERR

    git am -3 $BASE/$1 &> /dev/null
    status=$?
    if [ $status -eq 0 ] ; then
        echo -e "[$patchid] [\e[92mOK\e[0m] $1"
    else
        echo -e "[$patchid] [\e[91mEE\e[0m] $1"
        echo -e "cmd: git am -3 $BASE/$1"
        git am --abort
        exit 1
    fi
    patchid=$((patchid + 1))
}
