From 5c91672b5504f5ce1322c5d0ebbb0451d3977a3a Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?J=C3=A9r=C3=B4me=20Glisse?= <jglisse@redhat.com>
Date: Thu, 28 May 2020 14:34:27 -0400
Subject: [PATCH] fs/afs: use appropriate fs_page_index*() helper
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Use fs_or_swapfile_page_index() where appropriate otherwise use the
correct fs_page_index*() helper.

We need to use page_index() only for page that can be swap backed by a
file in the fs (such page are either anoymous page or shmem page and
thus do not belong properly to the fs and they can be seen by fs code
only through read_page() or direct_IO() code path).

Note that if swap backed page end up to be seen by a function where it
is not expected than there will be a WARN_ONCE() and we will use the
swap offset and not the page->index. So it should not regress and just
catch unexpected use.

Signed-off-by: Jérôme Glisse <jglisse@redhat.com>
Cc: David Howells <dhowells@redhat.com>
Cc: linux-afs@lists.infradead.org
Cc: linux-btrfs@vger.kernel.org
Cc: linux-fsdevel@vger.kernel.org
Cc: linux-mm@kvack.org
Cc: Alexander Viro <viro@zeniv.linux.org.uk>
Cc: Josef Bacik <jbacik@fb.com>
Cc: Tejun Heo <tj@kernel.org>
Cc: Jan Kara <jack@suse.cz>
Cc: Andrew Morton <akpm@linux-foundation.org>
---
 fs/afs/write.c | 13 +++++++------
 1 file changed, 7 insertions(+), 6 deletions(-)

diff --git a/fs/afs/write.c b/fs/afs/write.c
index cb76566763db..2aded3b808c6 100644
--- a/fs/afs/write.c
+++ b/fs/afs/write.c
@@ -314,6 +314,7 @@ static void afs_redirty_pages(struct writeback_control *wbc,
 static void afs_pages_written_back(struct afs_vnode *vnode,
 				   pgoff_t first, pgoff_t last)
 {
+	struct address_space *mapping = vnode->vfs_inode.i_mapping;
 	struct pagevec pv;
 	unsigned long priv;
 	unsigned count, loop;
@@ -329,14 +330,14 @@ static void afs_pages_written_back(struct afs_vnode *vnode,
 		count = last - first + 1;
 		if (count > PAGEVEC_SIZE)
 			count = PAGEVEC_SIZE;
-		pv.nr = find_get_pages_contig(vnode->vfs_inode.i_mapping,
-					      first, count, pv.pages);
+		pv.nr = find_get_pages_contig(mapping, first, count, pv.pages);
 		ASSERTCMP(pv.nr, ==, count);
 
 		for (loop = 0; loop < count; loop++) {
+			pgoff_t index = fs_page_index(mapping, pv.pages[loop]);
 			priv = page_private(pv.pages[loop]);
 			trace_afs_page_dirty(vnode, tracepoint_string("clear"),
-					     pv.pages[loop]->index, priv);
+					     index, priv);
 			set_page_private(pv.pages[loop], 0);
 			end_page_writeback(pv.pages[loop]);
 		}
@@ -778,10 +779,11 @@ vm_fault_t afs_page_mkwrite(struct vm_fault *vmf)
 	struct file *file = vmf->vma->vm_file;
 	struct inode *inode = file_inode(file);
 	struct afs_vnode *vnode = AFS_FS_I(inode);
+	pgoff_t index = fs_page_index(inode->i_mapping, vmf->page);
 	unsigned long priv;
 
 	_enter("{{%llx:%llu}},{%lx}",
-	       vnode->fid.vid, vnode->fid.vnode, vmf->page->index);
+	       vnode->fid.vid, vnode->fid.vnode, index);
 
 	sb_start_pagefault(inode->i_sb);
 
@@ -807,8 +809,7 @@ vm_fault_t afs_page_mkwrite(struct vm_fault *vmf)
 
 	priv = (unsigned long)PAGE_SIZE << AFS_PRIV_SHIFT; /* To */
 	priv |= 0; /* From */
-	trace_afs_page_dirty(vnode, tracepoint_string("mkwrite"),
-			     vmf->page->index, priv);
+	trace_afs_page_dirty(vnode, tracepoint_string("mkwrite"), index, priv);
 	SetPagePrivate(vmf->page);
 	set_page_private(vmf->page, priv);
 
-- 
2.26.2

