#!/bin/bash
self=`realpath -s $0`
base=`dirname $self`
blacklist=$1
dir=.

# Get all functions that have MAPPING_NULL but not a mapping or
# a mapping or an inode or a vam as one of its arguments
function funcs_which_need_mapping {
    spatch --include-headers --dir $dir --sp-file $base/tool-find-funcs-which-need-mapping.spatch \
           2> /dev/null > /tmp/unicorn-funcs-which-need-mapping

    # We need to use a black list because we definitly want to ignore
    # some function like *dump_page*()
    cat /tmp/unicorn-funcs-which-need-mapping | sort | uniq > /tmp/unicorn-tmp
    cp /tmp/unicorn-tmp /tmp/unicorn-funcs-which-need-mapping
}

# Recursively get all the functions that calls functions that
# have MAPPING_NULL
function funcs_and_callers {
    rm -f /tmp/unicorn-funcs
    cat /tmp/unicorn-funcs-which-need-mapping | sort | uniq | while read line ; do
        funcname=`echo $line | cut -d' ' -f1`
        filename=`echo $line | cut -d' ' -f2`
        if ! grep "^$funcname $filename" $blacklist > /dev/null ; then
            echo $line >> /tmp/unicorn-funcs
        fi
    done
    cat /tmp/unicorn-funcs > /tmp/unicorn-callers

    reccount=1
    echo Recurse $reccount

    while true; do
        rm -f /tmp/unicorn-tmp
        funcs_count=`wc -l /tmp/unicorn-funcs | cut -d' ' -f-1`
        cat /tmp/unicorn-callers | while read line ; do
            echo $line
            funcname=`echo $line | cut -d' ' -f1`
            filename=`echo $line | cut -d' ' -f2`
            if echo "$line" | grep static > /dev/null ; then
                $base/tool-find-funcs-calling-a-func.sh $funcname $filename >> /tmp/unicorn-tmp
            else
                $base/tool-find-funcs-calling-a-func.sh $funcname --dir $dir >> /tmp/unicorn-tmp
            fi
        done

        # black list
        rm -f /tmp/unicorn-callers
        cat /tmp/unicorn-tmp | sort | uniq | while read line ; do
            funcname=`echo $line | cut -d' ' -f1`
            filename=`echo $line | cut -d' ' -f2`
            if ! grep "^$funcname $filename" $blacklist > /dev/null ; then
                # Skip function we already have in our list
                if ! grep "^$funcname $filename " /tmp/unicorn-funcs > /dev/null ; then
                    echo $line >> /tmp/unicorn-funcs
                    echo $line >> /tmp/unicorn-callers
                fi
            fi
        done

        # Did we added any new functions ?
        count=`wc -l /tmp/unicorn-funcs | cut -d' ' -f-1`
        if [[ $funcs_count -eq $count ]]; then
            echo --------------------------------------------------------------
            echo Number of functions to add mapping parameter to: $funcs_count
            echo
            break
        fi

        reccount=$((reccount + 1))
        echo
        echo "Recurse $reccount (fcount: $funcs_count -> $count)"
    done
}

function generate_spatch {
    # First let's sort by directory to group things
    rm -f /tmp/unicorn-tmp
    cat /tmp/unicorn-funcs | while read line ; do
        filename=`echo $line | cut -d' ' -f2`
        echo filename $line >> /tmp/unicorn-tmp
    done

    cat /tmp/unicorn-tmp | sort | cut -d' ' -f2- | while read line ; do
        funcname=`echo $line | cut -d' ' -f1`
        filename=`echo $line | cut -d' ' -f2`
        prefix=`dirname $filename | sed -e "s:^./::" | sed -e "s:/:-:g"`

        echo Generate semantic patch to add mapping argument to $funcname

        # Create a semantic patch for that function
        if echo "$line" | grep static > /dev/null ; then
            filename=`echo "$filename" | sed -e 's:/:\\\/:g'`
            cat $base/tool-generate-add-mapping-spatch-static.template | \
            sed -e "s/DASUBERFUNCTIONNAME/$funcname/" | \
            sed -e "s/DASUBERFILENAME/$filename/" > \
            $base/tmp--$prefix--add-mapping-to-$funcname.spatch
        else
            cat $base/tool-generate-add-mapping-spatch-global.template | \
            sed -e "s/DASUBERFUNCTIONNAME/$funcname/" > \
            $base/tmp--$prefix--add-mapping-to-$funcname.spatch
        fi
    done
}


# main ------------------------------------------------------------------------
funcs_which_need_mapping
funcs_and_callers
#generate_spatch
