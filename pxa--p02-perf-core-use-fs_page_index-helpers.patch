From 229cb08e8f042b21db836c3513e7b7240a25517b Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?J=C3=A9r=C3=B4me=20Glisse?= <jglisse@redhat.com>
Date: Thu, 28 May 2020 14:49:46 -0400
Subject: [PATCH] perf/core: use fs_page_index*() helpers
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

For all pages that are file back we need to use fs_page_index*() to
access the struct page index field (this is done to support a generic
page protection mechanism).

Here we are dealing with something akin to a page allocated by a device
driver so we could also use raw_page_index(). Will need to make sure
page protection never accept such page.

Signed-off-by: Jérôme Glisse <jglisse@redhat.com>
Cc: Peter Zijlstra <peterz@infradead.org>
Cc: Ingo Molnar <mingo@redhat.com>
Cc: Arnaldo Carvalho de Melo <acme@kernel.org>
Cc: Mark Rutland <mark.rutland@arm.com>
Cc: Alexander Shishkin <alexander.shishkin@linux.intel.com>
Cc: Jiri Olsa <jolsa@redhat.com>
Cc: Namhyung Kim <namhyung@kernel.org>
Cc: linux-fsdevel@vger.kernel.org
Cc: linux-mm@kvack.org
Cc: Alexander Viro <viro@zeniv.linux.org.uk>
Cc: Josef Bacik <jbacik@fb.com>
Cc: Tejun Heo <tj@kernel.org>
Cc: Jan Kara <jack@suse.cz>
Cc: Andrew Morton <akpm@linux-foundation.org>
---
 include/trace/events/ext4.h      | 4 ++--
 include/trace/events/filemap.h   | 2 +-
 include/trace/events/writeback.h | 2 +-
 kernel/events/core.c             | 2 +-
 4 files changed, 5 insertions(+), 5 deletions(-)

diff --git a/include/trace/events/ext4.h b/include/trace/events/ext4.h
index db9f94735ed8..ff0586b5d4ac 100644
--- a/include/trace/events/ext4.h
+++ b/include/trace/events/ext4.h
@@ -553,7 +553,7 @@ DECLARE_EVENT_CLASS(ext4__page_op,
 	TP_fast_assign(
 		__entry->dev	= mapping->host->i_sb->s_dev;
 		__entry->ino	= mapping->host->i_ino;
-		__entry->index	= page->index;
+		__entry->index	= fs_page_index(mapping, page);
 	),
 
 	TP_printk("dev %d,%d ino %lu page_index %lu",
@@ -600,7 +600,7 @@ DECLARE_EVENT_CLASS(ext4_invalidatepage_op,
 	TP_fast_assign(
 		__entry->dev	= mapping->host->i_sb->s_dev;
 		__entry->ino	= mapping->host->i_ino;
-		__entry->index	= page->index;
+		__entry->index	= fs_page_index(mapping, page);
 		__entry->offset	= offset;
 		__entry->length	= length;
 	),
diff --git a/include/trace/events/filemap.h b/include/trace/events/filemap.h
index f033f2092fea..b4737f778988 100644
--- a/include/trace/events/filemap.h
+++ b/include/trace/events/filemap.h
@@ -29,7 +29,7 @@ DECLARE_EVENT_CLASS(mm_filemap_op_page_cache,
 	TP_fast_assign(
 		__entry->pfn = page_to_pfn(page);
 		__entry->i_ino = mapping->host->i_ino;
-		__entry->index = page->index;
+		__entry->index = fs_page_index(mapping, page);
 		if (mapping->host->i_sb)
 			__entry->s_dev = mapping->host->i_sb->s_dev;
 		else
diff --git a/include/trace/events/writeback.h b/include/trace/events/writeback.h
index 7fb2eb44907a..faf28366b535 100644
--- a/include/trace/events/writeback.h
+++ b/include/trace/events/writeback.h
@@ -69,7 +69,7 @@ DECLARE_EVENT_CLASS(writeback_page_template,
 			    bdi_dev_name(mapping ? inode_to_bdi(mapping->host) :
 					 NULL), 32);
 		__entry->ino = mapping ? mapping->host->i_ino : 0;
-		__entry->index = page->index;
+		__entry->index = fs_page_index(mapping, page);
 	),
 
 	TP_printk("bdi %s: ino=%lu index=%lu",
diff --git a/kernel/events/core.c b/kernel/events/core.c
index 633b4ae72ed5..5583e3f6144e 100644
--- a/kernel/events/core.c
+++ b/kernel/events/core.c
@@ -5715,7 +5715,7 @@ static vm_fault_t perf_mmap_fault(struct vm_fault *vmf)
 
 	get_page(vmf->page);
 	vmf->page->mapping = vmf->vma->vm_file->f_mapping;
-	vmf->page->index   = vmf->pgoff;
+	fs_page_index_set(vmf->vma->vm_file->f_mapping, vmf->page, vmf->pgoff);
 
 	ret = 0;
 unlock:
-- 
2.26.2

