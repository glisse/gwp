# !/bin/sh
self=`realpath -s $0`
base=`dirname $self`
input=$1

cat $input | grep -v "^-" | grep -v "^/" | grep -v "^$" | grep -v "./" > /tmp/unicorn-tmp
cat /tmp/unicorn-tmp | while read func ; do
    spatch -D fn=$func --dir . --no-includes --sp-file $base/tool-find-func.spatch 2> /dev/null
done
