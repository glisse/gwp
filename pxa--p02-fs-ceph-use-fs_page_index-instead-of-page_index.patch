From 2146944a285126b19da04cf55eaafba9ffa4f3ad Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?J=C3=A9r=C3=B4me=20Glisse?= <jglisse@redhat.com>
Date: Tue, 26 May 2020 14:38:42 -0400
Subject: [PATCH] fs/ceph: use fs_page_index*() instead of page_index()
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

page_index() is for page that can alias to a swap file (ie either an
anonymous page or shmem page that is swapped to a regular file on a
filesystem in which case the index correspond to the offset into the
swap file). A page for directory can not alias to swap so just use the
fs_page_index*() here.

Note that if swap backed page end up to be seen by a function where it
is not expected than there will be a WARN_ONCE() and we will use the
swap offset and not the page->index. So it should not regress and just
catch unexpected use.

Signed-off-by: Jérôme Glisse <jglisse@redhat.com>
Cc: Ilya Dryomov <idryomov@gmail.com>
Cc: Jeff Layton <jlayton@kernel.org>
Cc: ceph-devel@vger.kernel.org
Cc: Andrew Morton <akpm@linux-foundation.org>
Cc: Alexander Viro <viro@zeniv.linux.org.uk>
Cc: Josef Bacik <jbacik@fb.com>
Cc: Tejun Heo <tj@kernel.org>
Cc: Jan Kara <jack@suse.cz>
Cc: linux-fsdevel@vger.kernel.org
Cc: linux-mm@kvack.org
---
 fs/ceph/dir.c   | 3 ++-
 fs/ceph/inode.c | 2 +-
 2 files changed, 3 insertions(+), 2 deletions(-)

diff --git a/fs/ceph/dir.c b/fs/ceph/dir.c
index 4c4202c93b71..d800417a4d16 100644
--- a/fs/ceph/dir.c
+++ b/fs/ceph/dir.c
@@ -133,7 +133,8 @@ __dcache_find_get_entry(struct dentry *parent, u64 idx,
 	if (ptr_pos >= i_size_read(dir))
 		return NULL;
 
-	if (!cache_ctl->page || ptr_pgoff != page_index(cache_ctl->page)) {
+	if (!cache_ctl->page ||
+	    ptr_pgoff != fs_page_index(&dir->i_data, cache_ctl->page)) {
 		ceph_readdir_cache_release(cache_ctl);
 		cache_ctl->page = find_lock_page(&dir->i_data, ptr_pgoff);
 		if (!cache_ctl->page) {
diff --git a/fs/ceph/inode.c b/fs/ceph/inode.c
index 7fef94fd1e55..1f8c2840c08a 100644
--- a/fs/ceph/inode.c
+++ b/fs/ceph/inode.c
@@ -1554,7 +1554,7 @@ static int fill_readdir_cache(struct inode *dir, struct dentry *dn,
 	unsigned idx = ctl->index % nsize;
 	pgoff_t pgoff = ctl->index / nsize;
 
-	if (!ctl->page || pgoff != page_index(ctl->page)) {
+	if (!ctl->page || pgoff != fs_page_index(&dir->i_data, ctl->page)) {
 		ceph_readdir_cache_release(ctl);
 		if (idx == 0)
 			ctl->page = grab_cache_page(&dir->i_data, pgoff);
-- 
2.26.2

