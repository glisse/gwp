#!/bin/sh
SELF=`readlink -f $0`
BASE=`dirname $SELF`
source $BASE/tool-helpers.sh
if ! git diff-index --quiet HEAD; then
    echo "Git tree is not clean, abort !"
    exit -1
fi

patch pxa--p05-mm-fs-add-helper-to-test-page-mapping-including-if-p.patch
patch pxa--p05-mm-migrate-use-anon_or_fs_page_mapping_has.patch
patch pxa--p05-mm-memcontrol-use-anon_or_fs_page_mapping_has.patch
patch pxa--p05-mm-huge_memory-use-anon_or_fs_page_mapping_has.patch
patch pxa--p05-mm-memory-failure-use-anon_or_fs_page_mapping_has.patch
patch pxa--p05-mm-rmap-use-fs_page_mapping_has-and-fs_page_mapping_.patch
patch pxa--p05-mm-ksm-use-rawpage-helpers.patch
patch pxa--p05-mm-huge_memory-use-raw_page_mapping_check.patch
patch pxa--p05-mm-use-fs_page_mapping_has.patch
patch pxa--p05-gup-use-raw_page_mapping_has-to-check-if-page-has-so.patch
patch pxa--p05-fs-erofs-use-proper-helper-in-test-using-page-mappin.patch
patch pxa--p05-ext4-use-fs_page_mapping_check.patch
patch pxa--p05-f2fs-use-fs_page_mapping_check.patch
patch pxa--p05-gfs2-use-fs_page_mapping_check.patch
patch pxa--p05-fs-jbd2-use-fs_page_mapping_has.patch
patch pxa--p05-fs-splice-use-fs_page_mapping_has.patch
patch pxa--p05-fs-reiserfs-use-fs_page_mapping_has.patch
patch pxa--p05-fs-ceph-use-fs_page_truncated-fs_page_mapping_has.patch
patch pxa--p05-btrfs-use-raw_page_mapping_has-to-ascertain-page-map.patch
patch pxa--p05-fscrypt-use-raw_page_mapping_has-to-test-for-bounce-.patch
patch pxa--p05-fuse-use-raw_page_mapping_has-to-check-for-valid-pag.patch
patch pxa--p05-dax-use-fs_page_mapping_has-raw_page_mapping_has.patch
patch pxa--p05-futex-use-fs_page_mapping_has-and-fs_page_truncated.patch
patch pxa--p05-net-rds-use-anon_or_fs_page_mapping_has.patch
patch pxa--p05-mm-shmem-use-fs_page_mapping_check-fs_page_truncated.patch
patch pxa--p05-drivers-intel_th-use-raw_page_mapping_has.patch
spatch tool-spatch-cmds-2.sh pxa--p05-use-fs_page_is_truncated mapping
