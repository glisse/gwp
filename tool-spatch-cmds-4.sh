#!/bin/sh
spatch_file=$1

# P1 find files to change
spatch --dir . --include-headers -D part1 --sp-file $spatch_file

# P2 change all the files
cat /tmp/unicorn-files | sort | uniq | while read file ; do
    spatch --no-includes --in-place -D part2 --sp-file $spatch_file $file
done
