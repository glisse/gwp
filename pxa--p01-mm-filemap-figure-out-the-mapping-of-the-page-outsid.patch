From b4ee0639f887ddfdb117cfd0d08d5154c387076e Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?J=C3=A9r=C3=B4me=20Glisse?= <jglisse@redhat.com>
Date: Thu, 28 Feb 2019 15:50:13 -0500
Subject: [PATCH] mm/filemap: figure out the mapping of the page outside the
 trace code
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Figure out the mapping of a page outside the trace event code. This
is to remove all direct dereference of struct page mapping fields.

Signed-off-by: Jérôme Glisse <jglisse@redhat.com>
Cc: Andrew Morton <akpm@linux-foundation.org>
Cc: linux-mm@kvack.org
---
 include/trace/events/filemap.h | 20 ++++++++++----------
 mm/filemap.c                   |  7 ++++---
 2 files changed, 14 insertions(+), 13 deletions(-)

diff --git a/include/trace/events/filemap.h b/include/trace/events/filemap.h
index ee05db7ee8d2..96eb93ac153b 100644
--- a/include/trace/events/filemap.h
+++ b/include/trace/events/filemap.h
@@ -15,9 +15,9 @@
 
 DECLARE_EVENT_CLASS(mm_filemap_op_page_cache,
 
-	TP_PROTO(struct page *page),
+	TP_PROTO(struct address_space *mapping, struct page *page),
 
-	TP_ARGS(page),
+	TP_ARGS(mapping, page),
 
 	TP_STRUCT__entry(
 		__field(unsigned long, pfn)
@@ -28,12 +28,12 @@ DECLARE_EVENT_CLASS(mm_filemap_op_page_cache,
 
 	TP_fast_assign(
 		__entry->pfn = page_to_pfn(page);
-		__entry->i_ino = page->mapping->host->i_ino;
+		__entry->i_ino = mapping->host->i_ino;
 		__entry->index = page->index;
-		if (page->mapping->host->i_sb)
-			__entry->s_dev = page->mapping->host->i_sb->s_dev;
+		if (mapping->host->i_sb)
+			__entry->s_dev = mapping->host->i_sb->s_dev;
 		else
-			__entry->s_dev = page->mapping->host->i_rdev;
+			__entry->s_dev = mapping->host->i_rdev;
 	),
 
 	TP_printk("dev %d:%d ino %lx page=%p pfn=%lu ofs=%lu",
@@ -45,13 +45,13 @@ DECLARE_EVENT_CLASS(mm_filemap_op_page_cache,
 );
 
 DEFINE_EVENT(mm_filemap_op_page_cache, mm_filemap_delete_from_page_cache,
-	TP_PROTO(struct page *page),
-	TP_ARGS(page)
+	TP_PROTO(struct address_space *mapping, struct page *page),
+	TP_ARGS(mapping, page)
 	);
 
 DEFINE_EVENT(mm_filemap_op_page_cache, mm_filemap_add_to_page_cache,
-	TP_PROTO(struct page *page),
-	TP_ARGS(page)
+	TP_PROTO(struct address_space *mapping, struct page *page),
+	TP_ARGS(mapping, page)
 	);
 
 TRACE_EVENT(filemap_set_wb_err,
diff --git a/mm/filemap.c b/mm/filemap.c
index 5808968fb397..1cf361e953d0 100644
--- a/mm/filemap.c
+++ b/mm/filemap.c
@@ -232,7 +232,7 @@ void __delete_from_page_cache(struct page *page, void *shadow)
 {
 	struct address_space *mapping = page->mapping;
 
-	trace_mm_filemap_delete_from_page_cache(page);
+	trace_mm_filemap_delete_from_page_cache(mapping, page);
 
 	unaccount_page_cache_page(mapping, page);
 	page_cache_delete(mapping, page, shadow);
@@ -350,7 +350,8 @@ void delete_from_page_cache_batch(struct address_space *mapping,
 
 	xa_lock_irqsave(&mapping->i_pages, flags);
 	for (i = 0; i < pagevec_count(pvec); i++) {
-		trace_mm_filemap_delete_from_page_cache(pvec->pages[i]);
+		trace_mm_filemap_delete_from_page_cache(mapping,
+							pvec->pages[i]);
 
 		unaccount_page_cache_page(mapping, pvec->pages[i]);
 	}
@@ -901,7 +902,7 @@ static int __add_to_page_cache_locked(struct page *page,
 
 	if (!huge)
 		mem_cgroup_commit_charge(page, memcg, false, false);
-	trace_mm_filemap_add_to_page_cache(page);
+	trace_mm_filemap_add_to_page_cache(mapping, page);
 	return 0;
 error:
 	page->mapping = NULL;
-- 
2.21.0

