From 8fb6dbaa89ed896e763f8c90f54dee20fc5fad25 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?J=C3=A9r=C3=B4me=20Glisse?= <jglisse@redhat.com>
Date: Tue, 26 May 2020 12:13:14 -0400
Subject: [PATCH] mm/migrate: use fs_page_index*() or raw_page_mapping*()
 helpers (include bug fix)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

Fixing a bug when migrating shmem page that are backed by swap within a
swap file. In migrate_page_move_mapping() when we know it is a file back
page this imply it is not anonymous page swapped onto a file. If it is an
shmem page that is back by swap into a page then we _do not_ want to get
the index within the swap file (which is what page_index() would return)
but we want the index within the shmem file. Using the index within the
swap file means we are updating the wrong entry within the shmem xarray
(unless the index of the page and the swap index match by pure luck).

So use fs_page_index() and initialize xas accordingly.

Also page protected will have to through specific code path hence for
anonymous or file back page we can use the raw_page_index*() helpers.

Signed-off-by: Jérôme Glisse <jglisse@redhat.com>
Cc: Matthew Wilcox <willy@infradead.org>
Cc: Andrew Morton <akpm@linux-foundation.org>
Cc: Alexander Viro <viro@zeniv.linux.org.uk>
Cc: Josef Bacik <jbacik@fb.com>
Cc: Tejun Heo <tj@kernel.org>
Cc: Jan Kara <jack@suse.cz>
Cc: linux-fsdevel@vger.kernel.org
Cc: linux-mm@kvack.org
---
 mm/migrate.c | 19 +++++++++++--------
 1 file changed, 11 insertions(+), 8 deletions(-)

diff --git a/mm/migrate.c b/mm/migrate.c
index 7160c1556f79..d9f56a1fdcb9 100644
--- a/mm/migrate.c
+++ b/mm/migrate.c
@@ -220,7 +220,7 @@ static bool remove_migration_pte(struct page *page, struct vm_area_struct *vma,
 		if (PageKsm(page))
 			new = page;
 		else
-			new = page - pvmw.page->index +
+			new = page - raw_page_index(pvmw.page) +
 				linear_page_index(vma, pvmw.address);
 
 #ifdef CONFIG_ARCH_ENABLE_THP_MIGRATION
@@ -402,10 +402,10 @@ static int expected_page_refs(struct address_space *mapping, struct page *page)
 int migrate_page_move_mapping(struct address_space *mapping,
 		struct page *newpage, struct page *page, int extra_count)
 {
-	XA_STATE(xas, &mapping->i_pages, page_index(page));
+	int expected_count = expected_page_refs(mapping, page) + extra_count;
 	struct zone *oldzone, *newzone;
+	struct xa_state xas;
 	int dirty;
-	int expected_count = expected_page_refs(mapping, page) + extra_count;
 
 	if (!mapping) {
 		/* Anonymous page without mapping */
@@ -413,7 +413,7 @@ int migrate_page_move_mapping(struct address_space *mapping,
 			return -EAGAIN;
 
 		/* No turning back from here */
-		newpage->index = page->index;
+		raw_page_index_set(newpage, raw_page_index(page));
 		newpage->mapping = page->mapping;
 		if (PageSwapBacked(page))
 			__SetPageSwapBacked(newpage);
@@ -424,6 +424,9 @@ int migrate_page_move_mapping(struct address_space *mapping,
 	oldzone = page_zone(page);
 	newzone = page_zone(newpage);
 
+	XA_STATE_INIT(xas, &mapping->i_pages, fs_page_index(mapping, page),
+		      0, 0);
+
 	xas_lock_irq(&xas);
 	if (page_count(page) != expected_count || xas_load(&xas) != page) {
 		xas_unlock_irq(&xas);
@@ -439,7 +442,7 @@ int migrate_page_move_mapping(struct address_space *mapping,
 	 * Now we know that no one else is looking at the page:
 	 * no turning back from here.
 	 */
-	newpage->index = page->index;
+	raw_page_index_set(newpage, raw_page_index(page));
 	newpage->mapping = page->mapping;
 	page_ref_add(newpage, hpage_nr_pages(page)); /* add cache reference */
 	if (PageSwapBacked(page)) {
@@ -516,7 +519,7 @@ EXPORT_SYMBOL(migrate_page_move_mapping);
 int migrate_huge_page_move_mapping(struct address_space *mapping,
 				   struct page *newpage, struct page *page)
 {
-	XA_STATE(xas, &mapping->i_pages, page_index(page));
+	XA_STATE(xas, &mapping->i_pages, fs_page_index(mapping, page));
 	int expected_count;
 
 	xas_lock_irq(&xas);
@@ -531,7 +534,7 @@ int migrate_huge_page_move_mapping(struct address_space *mapping,
 		return -EAGAIN;
 	}
 
-	newpage->index = page->index;
+	raw_page_index_set(newpage, raw_page_index(page));
 	newpage->mapping = page->mapping;
 
 	get_page(newpage);
@@ -2071,7 +2074,7 @@ int migrate_misplaced_transhuge_page(struct mm_struct *mm,
 
 	/* anon mapping, we can simply copy page->mapping to the new page: */
 	new_page->mapping = page->mapping;
-	new_page->index = page->index;
+	raw_page_index_set(new_page, raw_page_index(page));
 	/* flush the cache before copying using the kernel virtual address */
 	flush_cache_range(vma, start, start + HPAGE_PMD_SIZE);
 	migrate_page_copy(new_page, page);
-- 
2.26.2

