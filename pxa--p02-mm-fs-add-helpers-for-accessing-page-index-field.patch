From bb9094475ef37947d3717805fccec8050831f4b5 Mon Sep 17 00:00:00 2001
From: =?UTF-8?q?J=C3=A9r=C3=B4me=20Glisse?= <jglisse@redhat.com>
Date: Sun, 24 May 2020 18:57:01 -0400
Subject: [PATCH] mm/fs: add helpers for accessing page->index field.
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit

This is to replace all direct dereference of struct page index field
by a call to the appropriate helper function. It makes it easier to
change rules regarding struct page->index field without having to
change all code accessing that field.

Signed-off-by: Jérôme Glisse <jglisse@redhat.com>
Cc: Zhang Yi <yi.z.zhang@intel.com>
Cc: Dave Hansen <dave.hansen@intel.com>
Cc: Thomas Gleixner <tglx@linutronix.de>
Cc: Mel Gorman <mgorman@techsingularity.net>
Cc: linux-fsdevel@vger.kernel.org
Cc: linux-mm@kvack.org
Cc: Alexander Viro <viro@zeniv.linux.org.uk>
Cc: Tejun Heo <tj@kernel.org>
Cc: Jan Kara <jack@suse.cz>
Cc: Josef Bacik <jbacik@fb.com>
Cc: Andrew Morton <akpm@linux-foundation.org>
---
 include/linux/mm_types.h |   8 +-
 include/linux/page-xa.h  | 294 +++++++++++++++++++++++++++++++++++++++
 2 files changed, 301 insertions(+), 1 deletion(-)

diff --git a/include/linux/mm_types.h b/include/linux/mm_types.h
index 4aba6c0c2ba8..9b98973b3da9 100644
--- a/include/linux/mm_types.h
+++ b/include/linux/mm_types.h
@@ -84,7 +84,13 @@ struct page {
 			struct list_head lru;
 			/* See page-flags.h for PAGE_MAPPING_FLAGS */
 			struct address_space *mapping;
-			pgoff_t index;		/* Our offset within mapping. */
+			/*
+			 * @index: Offset within mapping.
+			 *
+			 * DO NOT ACCESS DIRECTLY ! USE THE APPROPRIATE
+			 * *page_index* HELPERS ! See page-xa.h
+			 */
+			pgoff_t index;
 			/**
 			 * @private: Mapping-private opaque data.
 			 * Usually used for buffer_heads if PagePrivate.
diff --git a/include/linux/page-xa.h b/include/linux/page-xa.h
index 2d9456f811dc..33d6035b9a6f 100644
--- a/include/linux/page-xa.h
+++ b/include/linux/page-xa.h
@@ -63,4 +63,298 @@ static inline bool PageXA(struct page *page)
 }
 
 
+/*
+ * DO NOT ACCESS page->index DIRECTLY USE ONE OF THE BELOW HELPER. IF THE PAGE
+ * YOU ARE WORKING ON IS ANONYMOUS OR FILE BACK THEN YOU MUST USE THE CORRECT
+ * ANON OR FILE HELPER NEVER THE RAW VERSION !
+ */
+
+/**
+ * raw_page_index() - return the page->index field value for raw page.
+ *
+ * DO NOT USE WITH ANONYMOUS OR FILE BACK PAGE !
+ *
+ * A raw page is page that is not use as an anonymous page in a process or as a
+ * file back page.
+ *
+ * @page: pointer to page.
+ * @Return: page->index value.
+ */
+static inline pgoff_t raw_page_index(struct page *page)
+{
+	return page->index;
+}
+
+/**
+ * raw_page_index_set() - set the page->index field value for raw page.
+ *
+ * DO NOT USE WITH ANONYMOUS OR FILE BACK PAGE !
+ *
+ * A raw page is page that is not use as an anonymous page in a process or as a
+ * file back page.
+ *
+ * @page: pointer to page.
+ * @index: index value to set.
+ */
+static inline void raw_page_index_set(struct page *page, pgoff_t index)
+{
+	page->index = index;
+}
+
+/**
+ * anon_page_index() - get page offset of an anonymous page.
+ *
+ * USE ONLY WITH ANONYMOUS PAGE !
+ *
+ * An anonymous page is use in process anonymous vma (it can under protection
+ * in which case the same page might be use in multiple different vma and/or
+ * files, see the page protection documentation).
+ *
+ * @anon_vma: anon vma for which we want to get page offset of the page.
+ * @page: pointer to page.
+ * @Return: page offset value of the page against given anon vma.
+ */
+static inline pgoff_t anon_page_index(struct anon_vma *anon_vma,
+				      struct page *page)
+{
+	pgoff_t pgoff;
+
+	/* For exclusive access page this is harder ... */
+	if (PageXA(page)) {
+		/*
+		 * Marty this feature does not exist yet ...
+		 * Come back from the future !
+		 */
+		VM_BUG_ON_PAGE(PageXA(page), page);
+	}
+
+	if (!PageCompound(page)) {
+		return page->index;
+	}
+
+	pgoff = compound_head(page)->index;
+	pgoff += page - compound_head(page);
+	return pgoff;
+}
+
+/**
+ * anon_page_index_set() - set page offset of an anonymous page.
+ *
+ * USE ONLY WITH ANONYMOUS PAGE !
+ *
+ * See anon_page_index() for what an anonymous page is.
+ *
+ * @anon_vma: anon vma for which we want to set page offset for the page.
+ * @page: pointer to page.
+ * @pgoff: page offset value to set the page->index to for given anon vma.
+ */
+static inline void anon_page_index_set(struct anon_vma *anon_vma,
+				       struct page *page, pgoff_t pgoff)
+{
+	/* For exclusive access page this is harder ... */
+	if (PageXA(page)) {
+		/*
+		 * Marty this feature does not exist yet ...
+		 * Come back from the future !
+		 */
+		VM_BUG_ON_PAGE(PageXA(page), page);
+	}
+
+	page->index = pgoff;
+}
+
+extern pgoff_t swapfile_page_index(struct page *page);
+
+/**
+ * fs_page_index() - get page index (in page size) of a file back page.
+ *
+ * USE ONLY WITH FILE BACK PAGE !
+ *
+ * A file back page is use with file (it can under protection in which case the
+ * same page is might be use in multiple different different anonymous vma and/
+ * or files, see the page protection documentation).
+ *
+ * @mapping: mapping (file) for which we want to get page offset of the page.
+ * @page: pointer to page.
+ * @Return: page index value of the page against given mapping.
+ */
+static inline pgoff_t fs_page_index(struct address_space *mapping,
+				    struct page *page)
+{
+	struct page *page_head;
+	pgoff_t pgoff;
+
+	/* For exclusive access page this is harder ... */
+	if (PageXA(page)) {
+		/*
+		 * Marty this feature does not exist yet ...
+		 * Come back from the future !
+		 */
+		VM_BUG_ON_PAGE(PageXA(page), page);
+	}
+
+	/* This should not happen but handle it gracefuly for now. */
+	if (unlikely(PageSwapCache(page))) {
+		WARN_ONCE(1, "got swapbacked page please report !");
+		/*
+		 * Be graceful if we end up here correct behavior is to return
+		 * the offset within the swapfile as page is either anonymous
+		 * or shmem and thus page->index does not correspond to the
+		 * file we are dealing with.
+		 */
+		return swapfile_page_index(page);
+	}
+
+	/*
+	 * For non huge page or head page of huge page just return the index
+	 * field as it has the value we want. Note that here we do not care
+	 * about differencing between THP and hugetlbfs.
+	 */
+	if (likely(!PageTail(page))) {
+		return page->index;
+	}
+
+	page_head = compound_head(page);
+
+	/*
+	 * This is a tail page of a huge page 2 things we need to account:
+	 *   - THP does not set the page->index field of tail page so we have
+	 *     to compute it from the head page index value and the #pages
+	 *     between the head page and the page
+	 *   - Gigantic page (only hugetlbfs) can cross memory section in which
+	 *     case the struct page of the tail pages, might not all be at
+	 *     consecutive virtual addresses thus we can not use arithmetic on
+	 *     struct page pointer to get the index from the head page. Instead
+	 *     use pfn.
+	 *
+	 * One might wonder why not use pfn all the time ? Well page_to_pfn()
+	 * need a memory lookup with some memory model and thus it can be
+	 * costly.
+	 */
+	if (compound_order(page_head) >= MAX_ORDER)
+		pgoff = page_to_pfn(page) - page_to_pfn(page_head);
+	else
+		pgoff = page - page_head;
+
+	return pgoff + page_head->index;
+}
+
+/**
+ * fs_page_offset() - get page offset (in bytes) of a file back page.
+ *
+ * USE ONLY WITH FILE BACK PAGE !
+ *
+ * A file back page is use with file (it can under protection in which case the
+ * same page is might be use in multiple different different anonymous vma and/
+ * or files, see the page protection documentation).
+ *
+ * @mapping: mapping (file) for which we want to get page offset of the page.
+ * @page: pointer to page.
+ * @Return: page offset value of the page against given mapping.
+ */
+static inline loff_t fs_page_offset(struct address_space *mapping,
+				    struct page *page)
+{
+	return ((loff_t)fs_page_index(mapping, page)) << PAGE_SHIFT;
+}
+
+/**
+ * fs_page_index_set() - set page offset of a file back page.
+ *
+ * USE ONLY WITH FILE BACK PAGE !
+ *
+ * See fs_page_index() for what a file back page is.
+ *
+ * @mapping: mapping (file) for which we want to set page offset for the page.
+ * @page: pointer to page.
+ * @pgoff: page offset value for the page against given mapping.
+ */
+static inline void fs_page_index_set(struct address_space *mapping,
+				     struct page *page, pgoff_t pgoff)
+{
+	/* For exclusive access page this is harder ... */
+	if (PageXA(page)) {
+		/*
+		 * Marty this feature does not exist yet ...
+		 * Come back from the future !
+		 */
+		VM_BUG_ON_PAGE(PageXA(page), page);
+	}
+
+	page->index = pgoff;
+}
+
+/**
+ * fs_or_swapfile_page_index() - page (file back, or swap back) index.
+ *
+ * USE ONLY WITH FILE OR SWAP BACK PAGE !
+ *
+ * This helper provide common API to get the file offset for the page no matter
+ * if it is a swap back page (ie an anonymous or shmem page swapped to file on
+ * a filesystem) or a regular file back page.
+ *
+ * @mapping: mapping (file) for which we want to get page offset of the page,
+ *           if it is a swap back page then it is the mapping correspond to
+ *           the swap file.
+ * @page: pointer to page.
+ * @Return: page index (in PAGE_SIZE) against given mapping or swap file.
+ */
+static inline pgoff_t fs_or_swapfile_page_index(struct address_space *mapping,
+						struct page *page)
+{
+	if (unlikely(PageSwapCache(page))) {
+		/* Page under exclusive access should never be swapped ! */
+		VM_BUG_ON_PAGE(PageXA(page), page);
+		return swapfile_page_index(page);
+	}
+	return fs_page_index(mapping, page);
+}
+
+/**
+ * fs_or_swapfile_page_offset() - page (file back, or swap back) index.
+ *
+ * USE ONLY WITH FILE OR SWAP BACK PAGE !
+ *
+ * This helper provide common API to get the file offset for the page no matter
+ * if it is a swap back page (ie an anonymous or shmem page swapped to file on
+ * a filesystem) or a regular file back page.
+ *
+ * @mapping: mapping (file) for which we want to get page offset of the page,
+ *           if it is a swap back page then it is the mapping correspond to
+ *           the swap file.
+ * @page: pointer to page.
+ * @Return: page offset (in bytes) against given mapping or swap file.
+ */
+static inline pgoff_t fs_or_swapfile_page_offset(struct address_space *mapping,
+						 struct page *page)
+{
+	return ((loff_t)fs_or_swapfile_page_index(mapping, page)) <<
+		PAGE_SHIFT;
+}
+
+/**
+ * hugetlbfs_page_index() - page index (in page cache size) of hugetlbfs page.
+ *
+ * USE ONLY WITH HUGETLBS PAGE !
+ *
+ * @mapping: hugetlbfs mapping (file)
+ * @page: pointer to page.
+ * @Return: page index value of the page against given mapping.
+ */
+static inline pgoff_t hugetlbfs_page_index(struct address_space *mapping,
+					   struct page *page)
+{
+	/* Hugetlbfs does not support exclusive page access yet ! */
+	VM_BUG_ON_PAGE(PageXA(page), page);
+
+	if (!PageHeadHuge(page)) {
+		VM_BUG_ON_PAGE(1, page);
+		/* Assume it is not broken enough to be anon page. */
+		return fs_page_index(mapping, page);
+	}
+
+	return raw_page_index(page) << compound_order(page);
+}
+
+
 #endif /* LINUX_PAGE_XA_H */
-- 
2.26.2

