fs: use fs_page_index*() fs_page_offset*()

Use fs_page_index*()/fs_page_offset*() to replace all direct access of
page->index. All non fs access have already been converted to a proper
helper.

This patch does all the automatic conversion that can be done using the
following semantic patch:

Use following script (from root of linux kernel tree):

./that-script.sh that-semantic-patch.spatch

%<--------------------------------------------------------------------
-------------------------------------------------------------------->%

With the following semantic patch:

%<--------------------------------------------------------------------
-------------------------------------------------------------------->%

Signed-off-by: Jérôme Glisse <jglisse@redhat.com>
Cc: Andrew Morton <akpm@linux-foundation.org>
Cc: Alexander Viro <viro@zeniv.linux.org.uk>
Cc: Josef Bacik <jbacik@fb.com>
Cc: Tejun Heo <tj@kernel.org>
Cc: Jan Kara <jack@suse.cz>
Cc: linux-fsdevel@vger.kernel.org
Cc: linux-mm@kvack.org
